# Synthetic Data Factory

* [Overview of the SDF project](#overview-of-the-sdf-project)
* [Documentation](#documentation)
* [Contributing](#contributing)
* [Code of Conduct](#code-of-conduct)
* [License](#license)

## Overview of the SDF project

The Synthetic Data Factory (SDF) is a command line tool that has been developed by LNDS (Luxembourg National Data Service) to generate synthetic data (also known as artificial or mock-up data) based on different inputs such as real data or statistical distributions. 

It is an open-source initiative where everyone is welcome to contribute by potentially adding more features to the ever-growing dataset. It can be potentially extended to multiple domains where LNDS is active and might touch. Synthetic data is ideal to test systems, software, algorithms, data processing pipelines and visualizations. 

The SDF enables users to generate synthetic datasets from the Luxembourgish population based on the real statistical distribution structure. It provides a systematic generation of features which enrich a Luxembourgish cohort embedding the same statistical properties of the current Luxembourg population structure. The source used to build up the artificial dataset is STATEC [Link](https://lustat.statec.lu/?lc=en&ac=true), the government statistics service of Luxembourg.

The workflow of the SDF is represented in the following scheme:

![SDF schema](/SDF_structure_final.drawio_scheme_UPDATED_white.drawio.svg "SDF structure")

The dataset can be increased horizontally by adding more and more individual specifications and vertically by enriching with other people living at the country borders and working in Luxembourg, for instance. 

## Documentation

See the [Documentation](https://lnds-lu.gitlab.io/tooling/synthetic-data-factory) for more details and installation instructions.

## Contributing
We welcome contributions from everyone. For more details on how to contribute, including reporting bugs, proposing new features, and creating pull requests, please read our [Contributing Guidelines](./CONTRIBUTING.md).

## Code of Conduct
Our project is committed to fostering a welcoming community. We expect everyone to adhere to our [Code of Conduct](./CODE_OF_CONDUCT.md). By participating in this project, you agree to abide by its terms.

## License
This project is licensed under the Apache License 2.0. For more details, see the [LICENSE](./LICENSE) file.
