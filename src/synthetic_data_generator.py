#!.env/bin python3
"""
Synthetic Data Generation Module

A module to generate synthetic data from different input formats. Input can be a distribution or real data.
"""
from rules import CalculationRule
from distribution_generator import DistributionGenerator
from random_sampler import RandomSampler
from connector import Statec
import pandas as pd
import numpy as np 
import utilities as util
__version__ = '0.3.0'
__author__ = 'Kerstin Neininger', 'Silvia Martina'

import logging
logger = logging.getLogger(__name__)


class SyntheticDataGenerator:
    """
    A class to represent the worflow for real data request, synthetic data generation and export of dataset from the current 
    instruction of the pipeline 
    """

    def __init__(self, instructions: dict) -> None:
        """Constructor method.

        Args:
            instructions (dict): SDF instructions (configuration).
            domain (str): The domain data set for which synthetic data is generated. 
            target (str): The target data set for which synthetic data is generated in this domain.
            source_type (str): The source of the data (e.g. API request or previously calculated). Values are pre-defined, see README.
            source_data (str): An UUID referring to external resources (API requests).
            source_dependencies (list): The dependencies to other datasets.
            generator_method (str): Method how the synthetic data is generated. Values are pre-defined, see README.
            generator_model (str): Model how the synthetic data is generated. Values are pre-defined, see README.
            size_sample (int) : It is the size of sample.
            request_id (string): The UUID for the external dataset needed to ingest. 
            sdmx_statistics (pd.DataFrame): The SDMX statistics as returned by the API request.
            codelist (dict): The codelist of the SDMX statistics as returned by the API request.
            codelist_mapping (dict): The codelist mapping as returned by the API request.
            dependencies_data (dict): Data as calculated in previous steps are stored dependencies.
            synthetic_data (pd.DataFrame): The generated synthetic data based on the given instructions.
        """

        self.instructions = instructions
        self.target = self.instructions["target"]
        self.domain = self.instructions["domain"]
        self.source_type = self.instructions["source"]["type"]
        self.source_data = self.instructions["source"]["data"]
        self.source_dependencies = self.instructions["source"]["dependencies"]
        self.generator_method = self.instructions["generator"]["method"]
        self.generator_model=self.instructions["generator"]["model"]
        self.size_sample= self.instructions["generator"]["nrows"]
        self.request_id= self.instructions["source"]["data"]
        

        # Source data
        # self.domain = domain
        # self.target = target

        # Retrieved statistics and codelist based on domain and target
        # TODO generalize statistics as can also be real data
        self.sdmx_statistics = pd.DataFrame()
        # returned from pandasmdx and contains several series objects
        self.codelist = dict()
        # Dictionary with all codings based on the codelist
        # Example: {'L01': 'All ages', 'SL01': '0-4 years',...}
        self.codelist_mapping = dict() 

        self.dependencies_data = dict() #it is a dictionary since for any dependency is associated an dataset, here the datasets are   collected 

        # Synthetic data
        self.synthetic_data = pd.DataFrame()

    # TODO generalize, move to another class?
    def provide_source_data(self):
        """Retrieves source data information.

        The result depends on domain and target given but also on the source type (external or calculated).
        See README for more information.

        Raises:
            NotImplementedError: If the given source is not matching available implementations.
        """

        if self.source_type == "external" and len(self.source_dependencies)==0:
            # Request statistics or real data, e.g. via API call to STATEC
            # Statistic and codelist attributes are assigned after successful retrieval
            # "data": "b1504fde-6242-4240-b107-90e6ad8b014a",
            self.request_source_data()
            
        elif self.source_type == "external" and len(self.source_dependencies)!=0:
            
            self.request_source_data()
            for dependency in self.source_dependencies:
                filename = f"../static/sd_results/{dependency}.csv"
                df = self.open_source_data(filename)
                self.dependencies_data[dependency] = df
                
                
            
        
        elif self.source_type == "calculated":
            # "dependencies": ["person-age"]
            for dependency in self.source_dependencies:
                # TODO generalize, do not use path here?
                filename = f"../static/sd_results/{dependency}.csv"
                df = self.open_source_data(filename)
                self.dependencies_data[dependency] = df
        else:
            raise NotImplementedError("Chaining TODO.")

    # TODO currently STATEC object, need to generalize that
    # TODO move to another class?
    def request_source_data(self) -> None:
        """Requests source data from external resources.

        For STATEC: the number/statistics (=query) and the corresponding structure are retrieved.
        Structure pandas objects contain information on the metadata such as assignment of IDs to values/meaning, for example in the 
        gender-age data 'S02' means 'Males'.

        Args:
            request_id (str): UUID of the dataset
        """
        statec = Statec(self.request_id)

        # Check connection (takes some seconds, improvement needed)
        # logger.info(f"Connecting to Statec service")
        # print(statec.is_reachable())

        # Request data
        logger.info(f"Retrieve data for {self.request_id=}")
        statec.request_datasets()

        # Get query dataset (statistics i.e. numbers from external such as statec)
        # # Convert SDMX (result of the API request) to pandas dataframe
        self.sdmx_statistics = statec.get_sdmx_statistics() # 'the numbers'
        self.codelist_mapping = statec.get_codelist_mapping()   # 'the codes to the numbers'
        

        # Save to pkl (temp solution)
        ### remove later or move somewhere else ####
        id_scope = self.request_id + "_query"
        util.pandas_to_pickle(path=statec.LOCAL_EXPORT_PATH,
                            filename=id_scope, df=self.sdmx_statistics)
        util.pandas_to_csv(path=statec.LOCAL_EXPORT_PATH,
                            filename=id_scope, df=self.sdmx_statistics)
        df = util.pickle_to_pandas(path=statec.LOCAL_EXPORT_PATH, filename=id_scope)
        return df
        #print(df)
        ### remove later or move somewhere else ####

        # Retrieve structure, codelist, dataflow
        # statec.request_data(scope="structure")
        #self.codelist = statec.get_codelist()
        #structure = statec.get_structure()
        #dataflow = statec.get_dataflow()
        
        
        
        
        
        
        

    def open_source_data(self, filename: str) -> pd.DataFrame:
        """Opens the source data

        Args:
            filename (str): The filename including the path.

        Returns:
            pd.DataFrame: The source data as pandas object.
        """
        df = pd.read_csv(filename, sep=",")
        return df

   
            
            

    

    def apply_rule(self, source_data: pd.DataFrame, dependency: str) -> pd.DataFrame:
        """Apply a rule to calculate synthetic data from existing synthetic data.

        Args:
            source_data (pd.DataFrame): The existing dataset as basis for the calculation (dependency).
            dependency (str): The column in this dataset that is used as inout to apply the rule.

        Returns:
            pd.DataFrame: The dataset extended with the newly calculated values based on the applied rule.
        """
        # TODO generalize, new functions?
        # Matricule from dob
        rule = CalculationRule()
        df = rule.apply_rule_to_data(
            data=source_data, target=self.target, dependency=dependency)

        return df
    
    

    def generate_synthetic_data(self) -> None:
        """Generator of synthetic data.

        Args:
            nrows (int): The number of synthetically generated data
        """
        # try:
        # GATHER DATA
        self.provide_source_data()
        #dist_generator = DistributionGenerator(input={"statistics": self.sdmx_statistics, "codelist_mapping": self.codelist_mapping,"size_sample":self.size_sample})
        
        logger.info(
            f"Generating synthetic data with {self.generator_method=}.")
        # CALCULATE FROM DISTRIBUTION AND SAMPLING
        
        if self.domain=='person' and self.target =='age':
            
            pop_child=SyntheticDataGeneratorAge(self.instructions)
            pop_child.request_source_data()
            pop_child.calculate_distribution_age()
            self.synthetic_data=pop_child.synthetic_data
            
        elif self.generator_method == "sampling" and self.generator_model == "real_distr":
            
            if len(self.source_dependencies)!=0:
                
                if self.domain=='person' and self.target=='geo':
                    geo_child=SyntheticDataGeneratorDependency(self.instructions)
                    geo_child.provide_source_data()
                    dependency=geo_child.source_dependencies[0]
                    source_data_internal=geo_child.dependencies_data[f'{dependency}']
                    geo_child.geo_municipality(source_data_internal)
                    self.synthetic_data=geo_child.synthetic_data
                
            elif len(self.source_dependencies)==0:
                
                if self.domain=='person' and self.target=='nationality':
                    self.provide_source_data()
                    dist_generator_nat = DistributionGenerator(input={"statistics": self.sdmx_statistics, "codelist_mapping": 
                    self.codelist_mapping,"size_sample":self.size_sample})
                    self.synthetic_data=dist_generator_nat.nationality()
            else:
                raise AttributeError(f"{self.generator_method=} is invalid.")
                    
        elif self.generator_method == "rule":

            if len(self.source_dependencies)>1:
                dependency1 = self.source_dependencies[0]
                dependency2= self.source_dependencies[1]
                source_data1 = self.dependencies_data[dependency1]
                source_data2 = self.dependencies_data[dependency2]
                source_data=pd.concat([source_data1,source_data2],axis=1)
                dependency_column = dependency1.split("-")[1] 
                if self.domain=='person' and self.target=='name-surname':
                    self.synthetic_data = self.apply_rule(source_data=source_data, dependency=dependency_column)
                elif self.domain=='person' and self.target=='physical-attributes':
                    self.synthetic_data = self.apply_rule(source_data=source_data, dependency=dependency_column)
                else:
                    raise AttributeError(f"{self.generator_method=} is invalid.")    
                
            elif len(self.source_dependencies)==1:
                dependency = self.source_dependencies[0]# CALCULATE FROM SYNTHETIC DATA WITHOUT FURTHER SAMPLING
                
             # Example: age -> dob OR dob -> matricule
                # TODO assume currently that there is one dependency only, e.g. person-age
                source_data = self.dependencies_data[dependency]
                dependency_column = dependency.split("-")[1] 
                self.synthetic_data = self.apply_rule(source_data=source_data, dependency=dependency_column)
            else:
                raise AttributeError(f"{self.generator_method=} is invalid.")

        elif self.generator_method == "sampling" and self.generator_model=="statistics":

            if self.target=='salary':
                dependency = self.source_dependencies[0]
                print(dependency)
                source_data = self.dependencies_data[dependency]
                dependency_column = dependency.split("-")[1] 
                salary_distr=DistributionGenerator(input={"statistics": self.sdmx_statistics, "codelist_mapping": 
                self.codelist_mapping,"size_sample":self.size_sample})
                self.synthetic_data=salary_distr.salary(df_mun=source_data)

            else:
                raise AttributeError(f"{self.generator_model=} is invalid.")

            
        else:
            raise AttributeError(f"{self.generator_method=} is invalid.")

        # except Exception as err:
        #    logger.error(f"{err=}, {type(err)=}")
        #    # raise

    def export_data(self, file_name: str, format: str) -> None:
        """Exports synthetic data and saves information in a file.

        Args:
            file_name (str): The file name including the folder path
            format (str): The format in which the data is stored

        Raises:
            NotImplementedError: If the export format is not specified yet.
        """
        # Export data in preferred format
        if format == "csv":
            self.synthetic_data.to_csv(
                file_name, sep=',', encoding='utf-8', index=False)
        else:
            raise NotImplementedError("Export format not specified yet.")
            
            
            
            
            
            
            
class SyntheticDataGeneratorAge(SyntheticDataGenerator):
    """
    A subclass to generate the person-age dataset 
    """
    
    def __init__(self,instructions: dict)-> None:
        """Constructor method.

        Args:
            instructions (dict): SDF instructions (configuration).
            gender (str): gender property 
            age_range_M (array): the array with the minimum and maximum age for male
            age_range_F (array): the array with the minimum and maximum age for female
        """
        
        self.instructions = instructions
        self.gender= self.instructions["prop_pop"]["pop_gender"]
        self.age_range_M= np.array([self.instructions["prop_pop"]["age_min_M"],self.instructions["prop_pop"]["age_max_M"]])
        self.age_range_F= np.array([self.instructions["prop_pop"]["age_min_F"],self.instructions["prop_pop"]["age_max_F"]])
        super().__init__(instructions)
            

    def calculate_distribution_age(self) -> None:
        """function for calculating the synthetic age feature

        Args:
            instructions (dict): SDF instructions (configuration).
        """
        
        
        
        dist_generator = DistributionGenerator(input={"statistics": self.sdmx_statistics, "codelist_mapping": self.codelist_mapping,
        "size_sample":self.size_sample})
        self.synthetic_data =dist_generator.Age_dataset(self.gender,self.age_range_M,self.age_range_F)
              
        
        
class SyntheticDataGeneratorDependency(SyntheticDataGenerator):
    """
    A subclass for generating features which require additional information or dependencies
    """

    def __init__(self, instructions: dict) -> None:
        """Constructor method.

        Args:
            instructions (dict): SDF instructions (configuration).
        """

        self.instructions = instructions
        #self.source_data_internal=self.dependencies_data
        super().__init__(instructions)

    def geo_municipality(self, source_data_internal) -> None:
        """function for calculating the synthetic Municipality and Canton features

        Args:
            source_data_internal (pd.DataFrame): the internal data source dependency
        """

        dist_generator = DistributionGenerator(input={"statistics": self.sdmx_statistics, "codelist_mapping": self.codelist_mapping, "size_sample": self.size_sample})
        self.synthetic_data = dist_generator.municipality(source_data_internal)
