#!.env/bin python3
"""
Quality control Module

A module to ensure the quality of input and output data.

This includes:
    - Instruction files (.json) referred to in the command line
"""
from models import Instructions
from pydantic import ValidationError
import pydantic
__version__ = '0.1.0'
__author__ = 'Kerstin Neininger'

import logging
logger = logging.getLogger(__name__)


class QualityController:
    """
    A class to ensure quality of input and output.
    """

    def __init__(self) -> None:
        pass

    def validate_instructions(self, instructions: dict) -> bool:
        """Validates instructions given by a user.

        It uses the Pydantic package for data validation.

        Args:
            instructions (dict): The SDF piping instructions that were given an JSON input.

        Returns:
            bool: `True` if the given instructions are valid (as defined by the model), `False` otherwise.
        """
        try:
            Instructions(**instructions)
            return True
        except ValidationError as err:
            logger.error(f"{err=}, {type(err)=}")
            print(err.json(indent=4))
            return False
