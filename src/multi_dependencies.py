#!/usr/bin/env python3
"""Multi_dependencies Module

A module whic contains the functions for creating features depending on more than one other features. 

This includes:
    - name_surname function for generating fake names 
    -physical_attributes function for generating Ethnicity,Hair_color, Hair_length
"""
from faker import Faker 
import random
import pandas as pd
import numpy as np 
import copy

def name_surname(df_combined:pd.DataFrame) -> pd.DataFrame:

    """It is the function to add name and surnames to the synthetic dataset. 
        Args:
            df_combined(pd.Dataframe) : the part of the dataset containing the nationality and gender features.
        Returns:
            pd.DataFrame: the input datasets merged with the additional information about physical attributes. 
    """
    
    list_of_nationalities=['Luxembourg','Spain','Portugal','France','Italy','Belgium','Germany','United States of Ameria','Romania',
                       'Ukraine','Poland','India','Netherlands','United Kingdom','Brazil','Ireland','Hungary','Turkey','Sweden', 
                       'Bosnia and Herzegovina']
    
    #define a mapping between Nationality names and the local Faker provider 
    map_nat_lan = {"Luxembourg":['fr_FR','nl_BE'],"Spain":'es_ES',"Portugal":'pt_PT',"France":'fr_FR',"Italy":'it_IT', 
      "Belgium":'nl_BE', "Germany":'de_DE',"United States of Ameria":'en_US',"Romania":'ro_RO',"Ukraine":'uk_UA',"Poland":'pl_PL', 
     "India":'en_IN',"Netherlands":'nl_NL', "United Kingdom":'en',"Brazil":'pt_BR',"Ireland":'en_IE',"Hungary":'hu_HU', 
     "Turkey":'tr_TR',"Sweden":'sv_SE',"Bosnia and Herzegovina":'cs_CZ'}

    
    df_new=copy.deepcopy(df_combined)
    
    df_new["First_name"]=''  
    df_new["Surname"]=''
   

    for s in list_of_nationalities:
        df_one_nat=df_new[df_new["Nationality"]==s]
        fake=Faker(map_nat_lan[s], use_weighting=True)
        indeces=df_one_nat.iloc[:].index
        for index in indeces:
            last_name=fake.last_name()
            df_new.loc[index,"Surname"]=last_name
            
            if df_new.loc[index,'Gender']=='F':
                first_name=fake.first_name_female()
                df_new.loc[index,"First_name"]=first_name
                
            else:
                first_name=fake.first_name_male()
                df_new.loc[index,"First_name"]=first_name
            
    return df_new 
            


        
def physical_attributes(df_combined:pd.DataFrame) -> pd.DataFrame:

    """It is the function to add physical attributes: Ethnicity, hair_color, hair_lenght on the synthetic dataset. 
        Args:
            df_combined(pd.Dataframe) : the part of the dataset containing the nationality feature and the age feature 
        Returns:
            pd.DataFrame: the input datasets merged with the additional information about physical attributes. 
    """
    
    dict_ethnicity= {"Luxembourg":['white','latino','black','white','white','white','white'],"Spain":['white','white'],"Portugal":['white','white'],"France":['white','white','white','black'],"Italy":['white','white'],"Belgium":['white','white'],
               "Germany":['white','white'],"United States of Ameria":['white','latino','black','white'],"Romania":['white','white'],"Ukraine":['white','white'],"Poland":['white','white'],"India":['south asians','south asians'],"Netherlands":['white','white'],
               "United Kingdom":['white','asian','black','latino','white','white'],"Brazil":['latino','black'],"Ireland":['white','white'],"Hungary":['white','white'],"Turkey":['turkish'],"Sweden":['white','white'],"Bosnia and Herzegovina":['white','white']}
    list_hair_color=['brown','blond','black','gray','red']
    list_hair_lenght=['short','medium','long']
    df_new=copy.deepcopy(df_combined)
    df_new["Ethnicity"]='' 
    df_new["Hair_color"]=''
    df_new["Hair_length"]=''


    for i in range(len(df_new)):
        for j in dict_ethnicity:
            if df_new.loc[i,'Nationality']== j:
                df_new.loc[i,'Ethnicity']=random.choice(dict_ethnicity[j])
        df_new.loc[i,'Hair_length']=random.choice(list_hair_lenght)
        if df_new.loc[i,'Age'] in np.arange(0,25):
                    df_new.loc[i,'Hair_color']=np.random.choice(list_hair_color, p=[0.2, 0.2, 0.3, 0., 0.3])
        elif df_new.loc[i,'Age'] in np.arange(25,55):
                    df_new.loc[i,'Hair_color']=np.random.choice(list_hair_color, p=[0.2, 0.1, 0.4, 0.2, 0.1])
        elif df_new.loc[i,'Age'] in np.arange(55,70):
                    df_new.loc[i,'Hair_color']=np.random.choice(list_hair_color, p=[0.1, 0.1, 0.4, 0.4, 0.])
        elif df_new.loc[i,'Age'] in np.arange(70,100):
                    df_new.loc[i,'Hair_color']=np.random.choice(list_hair_color, p=[0., 0., 0.1, 0.9, 0.])
    return df_new












            
