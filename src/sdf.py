#!.env/bin python3
"""SDF Main Module

Synthetic Data Factory main module to generate synthetic data.

The Synthetic Data Factory (SDF) is a tool to generate synthetic/artificial/mock-up data based on different inputs.
Is is open source and an open data initiative to create synthetic data sets in the multiple domains.
"""
__version__ = '0.1.0'
__author__ = 'Kerstin Neininger'

import argparse
import json
import os
from datetime import datetime
import logging
import logging.config
from dotenv import find_dotenv, load_dotenv
import glob
import pandas as pd

# import configparser
# config = configparser.ConfigParser()
# config.read('../static/config/sdf-config-2023-05-17.ini')
# print(config['INPUT']['FileFormat'])

from pipeline import Pipeline
from synthetic_data_generator import SyntheticDataGenerator
from quality_control import QualityController
import utilities as util

# Load .env (logging)
load_dotenv()

CONFIG_DIR = "../config"    # Config
LOG_DIR = "../logs"         # Logging
# Directory for synthetic data output files
OUTPUT_DIR = "../static/sd_results/"


def _init_logger():
    """Load logging configuration dependent on the environment as specified in .env.

    Logger levels: DEBUG, INFO, WARNING, ERROR, CRITICAL
    """
    logging_configs = {"dev": "logging.dev.ini", "prod": "logging.prod.ini"}
    config = logging_configs.get(os.environ["ENV"], "logging.dev.ini")
    config_path = "/".join([CONFIG_DIR, config])
    timestamp = datetime.now().strftime("%Y%m%d-%H:%M:%S")

    logging.config.fileConfig(
        config_path,
        disable_existing_loggers=False,
        defaults={"logfilename": f"{LOG_DIR}/{timestamp}.log"},
    )


def run_sdf(instructions: dict) -> None:
    """Run the SDF and generate synthetic data based on the given instructions.

    Args:
        instructions (dict): The (piping) instructions (source data, models etc.)
    """
    try:
        logger.info(f"SDF started in {os.getcwd()}")

        # Quality control: Verify that JSON format is correct
        # TODO: might move to another function (especially if quality control)?
        logger.info("Quality control: STARTED.")
        qc = QualityController()
        valid = qc.validate_instructions(instructions=instructions)
        if not valid:
            raise ValueError(f"Instructions are not valid.")
        logger.info("Quality control: PASSED.")

        # Pipeline
        pipeline = Pipeline(instructions=instructions)
        logger.info(f"Pipeline progress: {pipeline.progress}%")
        pipeline.start()
        while not pipeline.reached_end:
            # print(pipeline.index)
            # print(pipeline.length)
            # print(pipeline.current_instruction)
            # print(pipeline.domain)
            # print(pipeline.current_target)
            current_instruction=pipeline.current_instruction
            domain = pipeline.current_domain
            target = pipeline.current_target
            nrows = pipeline.current_nrows
            logger.info(
                f"Generating synthetic data for {domain=}, {target=} ({nrows=})")

            # New generator
            # sd_generator = SyntheticDataGenerator(domain=domain, target=target)
            
            
            sd_generator = SyntheticDataGenerator(
                instructions=current_instruction)
            
            sd_generator.generate_synthetic_data()

            # Export
            # TODO export is mandatory at the moment as csv files are used for piping -> improve?
            sd_generator.export_data(
                file_name=f"{OUTPUT_DIR}{domain}-{target}.csv", format="csv")
            

            pipeline.next()
            logger.info(f"Pipeline progress: {pipeline.progress}%")
            
        logger.info(
            f"The generation of synthetic data has been COMPLETED SUCCESSFULLY. Synthetic data files were written in {OUTPUT_DIR=}")
        
        path='../static/sd_results/'
        csv_files=glob.glob(path +"*.csv")
        df_list = (pd.read_csv(file) for file in csv_files)
        big_df= pd.concat(df_list,axis=1)
        big_df=big_df.T.drop_duplicates().T
        big_df.to_csv(f"{path}syntheticdataset.csv", encoding='utf-8')
        logger.info(
            f"The generation of a single synthetic dataset has been written in {OUTPUT_DIR=}")
        
    except Exception as err:
            logger.error(f"{err=}, {type(err)=}")
            logger.error(
             "There was a problem in the SDF pipeline. Please check the error messages in the log file {LOG_DIR=}.")


def run_utilities(function: str) -> None:
    """Run the SDF utilities suite.

    Args:
        function (str): The name of SDF function from the utilities package.
    """
    print(function)
    try:
        logger.info(f"SDF utilities started in {os.getcwd()}")
        if function == "uuid":
            result = util.generate_uuid()
        
        logger.info(
            f"The utilities call for {function=} has been COMPLETED SUCCESSFULLY. The return value is the following {result=}")
    except Exception as err:
        # logger.error(f"{err=}, {type(err)=}")
        logger.error(
            "There was a problem in the SDF utilities. Please check the error messages in the log file {LOG_DIR=}.")


def main():
    """
    Examples:
        >>> python3 sdf.py --help

        # Might remove here - START
        >>> python3 sdf.py person -t gender
        >>> python3 sdf.py person -t gender -nrows 30
        # Might remove here - END

        >>> python3 sdf.py pipeline -i ../config/sdf-instructions-person-age-2023-05-19.json
        >>> python3 sdf.py pipeline -i ../config/sdf-instructions-person-chaining-2023-05-19.json
        >>> python3 sdf.py pipeline -i ../config/sdf-instructions-person-geo-2023-05-31.json
        >>> python3 sdf.py pipeline -i ../config/sdf-instructions-person-nationality-2023-07-14.json
    """
    parser = argparse.ArgumentParser()
    subparser = parser.add_subparsers(
        help="The area for which synthetic data is generated.", dest="command")

    # GENERAL
    # parser.add_argument("-nrows","--nbrrows", type=int, help="The number of synthetically generated data.", default=10) # For now, put that into the instructions
    # parser.add_argument("-o","--outfolder", type=int, help="The path to the output folder.", default="") # For now, define output folder automatically

    # CHAINING
    parser_pipeline = subparser.add_parser(
        "pipeline", help="Use multiple generators to chain and combine datasets.")
    parser_pipeline.add_argument("-i", "--instructions", type=str,
                                 help="The file path to the piping instructions. Instructions must be compliant with pre-defined format.", required=True)

    # UTILITIES
    # Generate UUID (used regularly, therefore available here)
    parser_utilities = subparser.add_parser(
        "utilities", help="Call functions that are used quite often.")
    parser_utilities.add_argument("-f", "--function", type=str,
                                 help="Functionality that should be used. There are several pre-defined choices.", required=True, choices=["uuid"])
    
    '''
    # Not needed anymore (part of the instructions)
    # PERSON
    parser_person = subparser.add_parser("person", help="Domain for identification data.")
    parser_person.add_argument("-t","--target", type=str, help="The target data set for which synthetic data is generated in this domain.", required=True, choices=["name", "gender", "age-dob", "matricule", "address"])

    # ENERGY (example only, not implemented)
    parser_energy = subparser.add_parser("energy", help="Domain for energy-related data.")
    parser_energy.add_argument("-t","--target", type=str, help="The target data set for which synthetic data is generated in this domain.", required=True, choices=["smartgrid", "tbd"])
    '''
    args = parser.parse_args()
    # RUN SDF pipeline (core feature)
    if args.command == "pipeline":
        with open(args.instructions) as file:
            instructions = json.load(file)
            run_sdf(instructions=instructions)
    
    # RUN SDF utilities
    if args.command == "utilities":
        run_utilities(function=args.function)

if __name__ == "__main__":
    _init_logger()
    logger = logging.getLogger(__name__)
    main()