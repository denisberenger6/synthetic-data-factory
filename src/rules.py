#!.env/bin python3
"""
Rules Module

A module to define rules and calculate synthetic data accordingly.
Examples:
- Calculate the date of birth from the age (which was calculated by `sampling` method)
- Generate the Luxembourg matricule from the date of birth
"""
import utilities as util
import datetime
import pandas as pd
import multi_dependencies
__version__ = '0.1.0'
__author__ = 'Kerstin Neininger'

import logging
logger = logging.getLogger(__name__)


class CalculationRule():
    """
     A class to define rules for calculating synthetic data from existing data and dependencies.

     It defines functions to generate the date of birth from the age, the social matricule number and the function to 
     generate new features from logic relationships.
    """

    def __init__(self) -> None:
        pass

    def dob_from_age(self, age: int) -> str:
        """Calculate the date of birth from the given age (in years).

        The day and the month are assigned randomly.

        Args:
            age (int): The age (in years).

        Returns:
            str: A random date of birth in ISO YYYYMMDD format with year calculated from age plus random month and day.

        Raises:
            ValueError: If `age` is outside the given range.
        """
        
        min = 0
        max = 120
        if age < min or age > max:
            raise ValueError(
                f"Age value must be in a range {min=} and {max=}, not {age=}.")
        else:
            now = datetime.datetime.now()
            current_year = now.year
            birth_year = current_year - age
            random_dob = util.generate_random_date(birth_year)
            return random_dob.strftime('%Y%m%d')

    def matricule_from_dob(self, dob: str) -> str:
        """Generate the Luxembourg matricule from the date of birth.

         Generated from the date of birth plus 4 random digits: YYYYMMDDxxxx*
         Per definition, leading zeros are allowed within the four random digits.

        Args:
            dob (str): Date of birth in the ISO format `YYYYMMDD`.

        Returns:
            str: A random Luxembourg matricule based on the `dob`.
        """
        random_digits = "".join(
            [str(util.generate_random_integer(0, 9)) for _ in range(4)])+"*"
        return f"{dob}{random_digits}"

    def apply_rule_to_data(self, data: pd.DataFrame, target: str, dependency: str) -> pd.DataFrame:
        """Apply the pre-defined rule to the provided dataset.

         The rule depends on the target and the dependency on other source datasets.

        Args:
            data (pd.DataFrame): The synthetic data to which the rules is applied.
            target (str): The target of the synthetic data that is calculated here.
            dependency (str): Other variables from the source data that are needed to calculate the target.

        Returns:
            pd.DataFrame: The dataset extended with the newly calculated values based on the applied rule.
        """
        try:
            if target == "dob" and dependency == "age":
                data['Date_of_birth'] = data.apply(
                    lambda row: self.dob_from_age(row["Age"]), axis=1)
                
            elif target == "matricule" and dependency == "dob":
                data['Social_matricule'] = data.apply(
                    lambda row: self.matricule_from_dob(row["Date_of_birth"]), axis=1)
                
            elif target== "name-surname" and dependency == "nationality" :
                data=multi_dependencies.name_surname(data)                
            elif target=="physical-attributes" and dependency == "age":
                data=multi_dependencies.physical_attributes(data)
                
                
            return data
        except Exception as err:
            logger.error(f"{err=}, {type(err)=}")
