#!.env/bin python3
"""
Distribution Generator Module
A module to generate data distributions from different input formats. Input can be a statistics or real data.
"""
__version__ = '0.1.0'
__author__ = 'Kerstin Neininger, Silvia Martina'

import logging
logger = logging.getLogger(__name__)
import numpy as np 
import pandas as pd
from random_sampler import RandomSampler
from random_sampler import Statistics 
import random as rnd
import math as mt
import copy
from scipy.stats import norm
pd.options.mode.chained_assignment = None

class DistributionGenerator:
    """
    A class to extract the input data from the imported dataset for its synthetic distribution generation from the specific dataset 
    """
    def __init__(self,input:dict) -> None:
        """Constructor method.
    

        Args:
             input (dict) : input["statistics"] contains the dataset to be used for generating the next feature.
                            input["codelist_mapping"] contains the mapping list to label the strings associated with meaningful words.
                            input["size_sample"] is the sample size.   
        """
        self.dataset=input["statistics"]
        self.mapping_list=input["codelist_mapping"]
        self.nrows=input["size_sample"]
   
    
    def Age_dataset(self,gender,age_range_M,age_range_F)-> pd.DataFrame:

        """It is the first function to be used in the SDF pipeline for creating the core-base dataset with gender and age features for 
        each row which represents the single person profile. 

        Args:
            gender: string contains "M" , "F" or mixed to specified the population gender characteristic. 
            age_range_M: array of two integers with the lowest and the biggest age of the male population. 
            age_range_F: array of two integers with the lowest and the biggest age of the female population. 

        Returns:
            pd.DataFrame: the first dataset you can create with the SDF pipeline. 
        """
        
        
         
        df_new=self.dataset
        df2=copy.deepcopy(df_new)
            
        if gender == "M":
            df = df2[df2.SEX.str.contains('A02',case=False)]# selection of MALES 
            df = df[df.AGE.str.contains('SL',case=False)].sort_values('AGE',ascending=True)  #sorting AGE column by increasing order 
            df = df[age_range_M[0]:age_range_M[1]+1] #data filtering by the range 
            df['synth value'] = self.random_sampling(df['value'])
            df_synth=df[['SEX','AGE','synth value']]
            
                
                        
        elif gender=="F":
            
            df = df2[df2.SEX.str.contains('A03',case=False)] # selection of FEMALES
            df = df[df.AGE.str.contains('SSL',case=False)].sort_values('AGE',ascending=True)  #sorting AGE column by increasing order 
            df = df[age_range_F[0]:age_range_F[1]+1]#data filtering by the range
            df['synth value'] = self.random_sampling(df['value'])
            df_synth=df[['SEX','AGE','synth value']]
                
            
        elif gender=='mixed':
            
            df_M = df2[df2.SEX.str.contains('A02',case=False)]
            df_M=df_M[df_M.AGE.str.contains('SSL',case=False)].sort_values('AGE',ascending=True) 
            df_M= df_M[age_range_M[0]:age_range_M[1]+1]
            df_F = df2[df2.SEX.str.contains('A03',case=False)]
            df_F=df_F[df_F.AGE.str.contains('SSL',case=False)].sort_values('AGE',ascending=True)
            df_F=df_F[age_range_F[0]:age_range_F[1]+1]
            df_Tot_num = pd.DataFrame([np.sum(df_M.loc[:,'value']),np.sum(df_F.loc[:,'value'])], columns=['size']) #intermediate dataset with the real portion of male and female for the selected age range


            sample_gender_tot=self.random_sampling(df_Tot_num['size'])
            #samples_sex[0] wil be the total number of male population within synth population 
            #samples_sex[1] wil be the total number of female population within synth population 
            sample_M=RandomSampler(df_M['value'],sample_gender_tot[0]).get_random_sample_from_distribution()
            sample_F=RandomSampler(df_F['value'],sample_gender_tot[1]).get_random_sample_from_distribution()
            df_M['synth value']=sample_M
            df_F['synth value']=sample_F
            df_F_final=df_F[['SEX','AGE','synth value']]
            df_M_final=df_M[['SEX','AGE','synth value']]
            df_synth=pd.merge(df_M_final,df_F_final,on=['SEX','AGE','synth value'],how='outer').replace(np.nan,0)
                
                
        df_synth_final=self.Transform_DataFrame(df_synth)
        
        df_synth_final=df_synth_final.sample(n=len(df_synth_final))
              
             
        return df_synth_final
          
          
            
    def Transform_DataFrame(self,Dataframe) -> pd.DataFrame:


        """It is a function to convert the DataFrame where `synth value` column is the number of people sharing the same age and 
        gender into a longitudinal dataset in which there is only `gender` and `age` information for each row corresponding to a 
        single profile.  

        Args:
            pd.Dataframe: the initial created dataset with `AGE`,`SEX` and,`synth value` column corresponding to the number of people  
            with the same age and gender. 
        

        Returns:
            pd.DataFrame: the first dataset of SDF with `Age` and `Gender` as columns and the rows corresponding to a single synthetic 
            profile. 
        """


        
        Dataframe['AGE']=Dataframe['AGE'].map(self.mapping_list['name'].to_dict())
        Dataframe['AGE'] = Dataframe['AGE'].str.extract('(\d+)', expand=False)
        Dataframe.rename(columns = {'AGE':'Age'}, inplace = True) 
        Dataframe['SEX'] = Dataframe['SEX'].replace('A02', 'M')
        Dataframe['SEX'] = Dataframe['SEX'].replace('A03', 'F')
        Dataframe.rename(columns = {'SEX':'Gender'}, inplace = True) 
        NewDataFrame=Dataframe.loc[Dataframe.index.repeat(Dataframe['synth value'])].reset_index(drop=True)
        NewDataFrame=NewDataFrame.drop(['synth value'],axis=1)
        return NewDataFrame      

    def random_sampling(self,realdistr: pd.DataFrame) -> pd.DataFrame:

        """ Random sampling from a real descrete distribution.

        Args:
           realdistr (pd.DataFrame): The reconstructed real discrete histogram of data.

        Returns:
           pd.DataFrame: The randomly generated sample drawn by the real data dustribution. 
        """
        sampler = RandomSampler(realdistr,self.nrows)
        df_synth = sampler.get_random_sample_from_distribution()
        return df_synth      
                            
     
                
    def municipality(self,df_age: pd.DataFrame)-> pd.DataFrame:

        """This function creates the column `Municipality` in which each profile in the already generated dataset, containing gender 
        and age (df_age), are associated to a Luxembourgish municipality. 

        Args:
           
           df_age(pd.DataFrame): The previous generated person-age dataset 

        Returns:
           pd.DataFrame: The df_age dataset with a new column called "Municipality" added to it. 
        """
        
        df_new =self.dataset 
        df_mun=copy.deepcopy(df_new)
        df_mun['MUNICIPALITY NAMES'] = df_mun['SPECIFICATION'].map(self.mapping_list['name'].to_dict())

        df_mun=df_mun.sort_values(by='value', ascending=False) #to sort dataframe by SPECIFICATION in an increasing order which respects cantons

        
         #the variable `matrix_moyen_age_range` contains the information about the moyen age-range per municipality as it is reported on a color map in a figure in the statec report: "The territorial distribution of the population "                                        
             
        matrix_moyen_age_range=[[36.0, 37.5],[37.6,39.0], [39.1,40.5],[40.6,42.0], [42.1,45.2]]; # -> 5 intervals for the average range age
         #age_average_level =[1,2,3,4,5]; #each entry is referred to a range 
        #create a mapping between SPECIFICATION column and age_average_level entries. 
        map_age = {"CM01001":3, "CM01002":2,"CM01003":3,"CM01004":4,"CM01005":3, "CM02001":3,"CM02002":2,"CM02003":5,"CM02004":5,
        "CM02005":4, "CM02006":2,"CM02007":2, "CM02008":2,"CM02009":2,"CM02010":2,"CM03001":3 ,"CM03002":3,"CM03003":2 ,"CM03004":3,
        "CM03005":3,"CM03006":4,"CM03007":1,"CM03008":3,"CM03009":2,"CM03010":2, "CM04001":1, "CM04002":2,"CM04003":4,"CM05001": 3,  
        "CM05002":2,"CM05003":3, "CM05004":3,"CM05005":3,"CM05006":1,"CM05007": 5,"CM06001":2,"CM06002":4,"CM06003":3,"CM06004":3,
        "CM06005":5, "CM06006":3,"CM06007":1, "CM07001": 2,"CM07002":3,"CM07003": 3, "CM07004":4,"CM07005":3, "CM07006":4,"CM07007":4,
        "CM07008":4,"CM08001":3, "CM08002":3, "CM08003":2,"CM08004":5,"CM08005":5, "CM08006":4,"CM08007":4, "CM08008": 3, "CM09001":4, 
        "CM09002":3, "CM09003": 3,"CM09004":5,"CM09005":4,"CM09006":3,"CM09007":3,"CM09008":3,"CM09009":4, "CM10001":4,"CM10002":1,
        "CM10003":4,"CM10004":3, "CM10005":3, "CM10006": 3, "CM10007": 4,"CM10008":5,"CM10009":2,"CM10010":3,"CM10011":4,"CM10012":2,
        "CM10013":3, "CM10014":3,"CM11001":4,"CM11002":4,"CM11003":3,"CM11004":2,"CM11005":5,"CM11006":3 ,"CM11007":3,"CM11008":5,
        "CM11009":2,"CM11010":3,"CM11011":2,"CM12001":3,"CM12002":2 , "CM12003":1 ,"CM12004":1,"CM12005": 2,"CM12006":2,"CM12007" :2,
        "CM12008" :4 ,"CM12009":3,"CM12010":2}


        keys = ["CM01001", "CM01002","CM01003","CM01004","CM01005", "CM02001","CM02002","CM02003","CM02004","CM02005", 
                  "CM02006","CM02007", "CM02008","CM02009","CM02010","CM03001" ,"CM03002","CM03003" ,"CM03004","CM03005","CM03006",
                  "CM03007","CM03008","CM03009","CM03010", "CM04001", "CM04002","CM04003","CM05001", "CM05002","CM05003", "CM05004",
                  "CM05005","CM05006","CM05007","CM06001","CM06002","CM06003","CM06004","CM06005", "CM06006","CM06007", "CM07001",
                  "CM07002","CM07003", "CM07004","CM07005", "CM07006","CM07007","CM07008","CM08001", "CM08002", "CM08003","CM08004",
                  "CM08005", "CM08006","CM08007", "CM08008", "CM09001", "CM09002", "CM09003","CM09004","CM09005","CM09006","CM09007",
                  "CM09008","CM09009", "CM10001","CM10002","CM10003","CM10004", "CM10005", "CM10006", "CM10007","CM10008","CM10009",
                  "CM10010","CM10011","CM10012","CM10013", "CM10014","CM11001","CM11002","CM11003","CM11004","CM11005","CM11006" ,
                  "CM11007","CM11008","CM11009","CM11010","CM11011","CM12001","CM12002" , "CM12003" ,"CM12004","CM12005","CM12006",
                  "CM12007","CM12008" , "CM12009","CM12010"]
        
        myDict_canton = {}
        myDict_canton['Canton Luxembourg']=['Bertrange','Contern','Hesperange','Luxembourg','Niederanven','Sandweiler','Schuttrange',  
        'Steinsel','Strassen','Walferdange','Weiler-la-Tour']
        myDict_canton['Canton Esch']=['Bettembourg','Differdange','Dudelange','Esch-sur-Alzette','Frisange','Kayl','Leudelange',
        'Mondercange','Pétange','Reckange-sur-Mess','Roeser','Rumelange','Sanem','Schifflange']
        myDict_canton['Canton Capellen']=['Dippach','Garnich','Habscht','Käerjeng','Kehlen','Koerich','Kopstal','Mamer','Steinfort']
        myDict_canton['Canton Mersch']=['Bissen','Colmar-Berg','Fischbach','Heffingen','Helperknapp','Larochette','Lintgen',
        'Lorentzweiler','Mersch','Nommern']
        myDict_canton['Canton Diekirch']=['Bettendorf','Bourscheid','Diekirch','Erpeldange-sur-Sûre','Ettelbruck','Feulen','Mertzig',
        'Reisdorf','Schieren',"Vallée de l'Ernz"]
        myDict_canton['Canton Grevenmacher']=['Betzdorf','Biwer','Flaxweiler','Grevenmacher','Junglinster','Manternach','Mertert',
        'Wormeldange']
        myDict_canton['Canton Remich']=['Bous-Waldbredimus','Dalheim','Lenningen','Mondorf-les-Bains','Remich','Schengen',
        'Stadtbredimus','Bous']
        myDict_canton['Canton Redange']=['Beckerich','Ell','Grosbous','Préizerdaul','Rambrouch','Redange-sur-Attert','Saeul',
        'Useldange','Vichten','Wahl']
        myDict_canton['Canton Clervaux']=['Clervaux','Parc Hosingen','Troisvierges','Weiswampach','Wincrange']
        myDict_canton['Canton Echternach']=['Beaufort','Bech','Berdorf','Consdorf','Echternach','Rosport-Mompach','Waldbillig']
        myDict_canton['Canton Wiltz']=['Boulaide','Esch-sur-Sûre','Goesdorf','Kiischpelt','Lac de la Haute-Sûre','Wiltz','Winseler']
        myDict_canton['Canton Vianden']=['Vianden','Tandel','Putscheid']
         #create a column called "ÂGE MOYEN" 
        df_mun["Age Moyen range"] = df_mun["SPECIFICATION"].map(map_age)

        df_mun['value'] = df_mun['value'].apply(lambda x: x/df_mun[df_mun['SPECIFICATION']=='LU'].value) #normalisation of the value column respect to total Lux population to normalise the frequencies  
        df_mun= df_mun[df_mun.SPECIFICATION.str.contains('CM',case=False)].sort_values('value',ascending=False)
        tot_mun=df_mun['SPECIFICATION'].str.contains('CM').sum(); #total number of municipalities 
        df_mun=df_mun.reset_index()
        df_age_new=copy.deepcopy(df_age)
        df_age_new["Municipality"]='' #Municipality column from the previous dataset
         #we can think to reduce the municipalities listed in the map_age & Keys 
        def Canton(municipality):
            """It is a function to assign canton to municipality. 
            Args:
            municipality : string of municipality name in in the my_Dict_canton dictionary. 

            Returns:    
            canton : string of the canton name where the municipality belongs. 
            """ 
            for i in myDict_canton.keys():
                if municipality in myDict_canton[i]:
                    canton=i
                    return canton 
         

         
        def municipality_inference(sd,top_mun)-> pd.DataFrame:
            """It is a function to assign municipality to each profile based on Bayes' theorem. The variability of the outcome can be 
            tuned by the function inputs.  
            Args:
                 sd (int) : integer of the standard deviation of the normal distribution we reconstructed around a mean value. Bigger 
                             this  parameter is, more spread out are the age values around the mean. 
                 top_mun (int) : integer of the first `top_mun` municipality list with the highest probability for its municipality to 
                                  be associated to the profile with a specific age. 

            Returns:    
            pd.Dataframe: the df_age dataset with the `municipality` included. 
            """ 

            for i in range(len(df_age_new)):
                P_MA=np.zeros(len(keys))
                age_i= df_age_new.loc[i,'Age'] #age for each i to start from 1 add +1
                P_age=df_age_new['Age'].value_counts()[age_i]/len(df_age_new) #it is the probability for a person with a specific age to belong at synthetic population : Priori Age probability 
                for j in range(len(keys)):
                    min_age=matrix_moyen_age_range[int(df_mun[df_mun["SPECIFICATION"] == keys[j]]["Age Moyen range"].iloc[0])-1][0];
                    max_age=matrix_moyen_age_range[int(df_mun[df_mun["SPECIFICATION"] == keys[j]]["Age Moyen range"].iloc[0])-1][1];
                    mean=rnd.uniform(min_age,max_age) 
                    #it is the mean of the distrubution that is chosen from a uniform distribution in the range [min_age,max_age] domain  
                    P_AM= norm.cdf(age_i +1, mean,sd) - norm.cdf(age_i-1, mean, sd)
                    P_municipality=df_mun[df_mun["SPECIFICATION"] == keys[j]]["value"].iloc[0] # it is the array with the priori probability associated to each municipality 
                    P_MA[j]=P_AM*P_municipality/P_age;#this array is ordered respect to the key list. For each municip, it contains the prob associated to the age.
                pos_mun=np.argpartition(P_MA, -top_mun)[-top_mun:] #returns the INDEX of the top municipalities of the array P_MA 
                P_MA_red=P_MA[pos_mun]#selection of the probability array to the top_mun municipalities 
                selected_mun=pos_mun[RandomSampler(P_MA_red,1).get_random_sample_from_distribution().astype(int).tolist().index(1)] #randomly only ONE 1 is in the 
                 # output. with .index(1) , we identified the position in pos_mun. 
                mun_feature=keys[selected_mun]# return the random choice of the list of the top_mun with highest probability 
                df_age_new.loc[i,"Municipality"]= df_mun.loc[df_mun["SPECIFICATION"] == mun_feature, "MUNICIPALITY NAMES"].iloc[0]
                
            return df_age_new



        df_synth=municipality_inference(70,90) #generation of the dataset with municipality included 
        df_synth["Municipality"]=df_synth["Municipality"].replace('Rosport - Mompach','Rosport-Mompach')
        df_synth["Canton"]=df_synth["Municipality"].apply(Canton)
         #top_mun from df_syn created 
         #top_mun_list=df_synth.Municipality.unique() #top_mun from df_syn created 

        return df_synth 
            
    
    def nationality(self)-> pd.DataFrame:

         """attributes a nationality to each entry

         Args: no additional dataset is required since the nationality is associated independently from the rest of the dataset 
               

         Returns:
           pd.DataFrame: The column "Nationality" with length equal to the size of the sample 
         """
         df =self.dataset
        
            #it could be good using the mapping list to add a column of nationality refering to the labels in POPULATION column OR
            #substitute it with the relating nationality 
         df['POPULATION'] = df['POPULATION'].map(self.mapping_list['name'].to_dict())

         df = df.sort_values('value',ascending=False).iloc[0:40,:][['value','POPULATION']]

         """ here some information we would know 
         Total_Lux_pop= df.loc[df['POPULATION'] =="TOTAL POPULATION", 'value'].iloc[0]
         Lux_pop= df.loc[df['POPULATION'] =="Luxembourg", 'value'].iloc[0]
         Forign_pop= df.loc[df['POPULATION'] =="Total foreigners", 'value'].iloc[0]
         """

         #drop from df the entries relating to total Population and other countries 
             
         nationality_excluded = ['TOTAL POPULATION','Total foreigners','EUROPE','Other european countries','Other countries EU','Asia',
         'Other no-european countries','ASIA','South America','China','Greece','Syria','Montenegro','North America','Cape Verde', 
         'Russian Federation','State of Eritrea','Bulgaria',' Bosnia and Herzegovina','Serbia','Morocco','AMERICA','AFRICA']
        
         df_nationality = df.loc[df['POPULATION'].isin(nationality_excluded)== False].reset_index(drop=True)
             
         sample_nationality=self.random_sampling(df_nationality['value']) 
             
            #HERE TO ASSIGN THE NATIONALITY RANDOMLY TO EACH SYNTHETIC INDIVIDUAL


         df_nat= pd.DataFrame(index=np.arange(self.nrows))
         df_nat['Nationality']=''
         df_nat.loc[0:np.cumsum(sample_nationality)[0]]=df_nationality.loc[0]['POPULATION']  #
         for i in range(len(sample_nationality)-1):
            df_nat.loc[np.cumsum(sample_nationality)[i]:np.cumsum(sample_nationality)[i+1]]=df_nationality.loc[i+1]['POPULATION']  
                    
            #synthetic Nationality columns 
         df_nat=df_nat.sample(n=self.nrows) 
            #this column can be add to the synthetic dataset. 

         df_synth=df_nat
         return df_synth 

    
    
    def salary(self,df_mun: pd.DataFrame)-> pd.DataFrame:
           
         """ It is the function to generate salary attribute based on statistics information for each municipality 

        Args:
           df_mun (pd.DataFrame): The already generated dataset containing Municipality column.

        Returns:
           pd.DataFrame: The input dataset with the Salary feature included 
        """


         df =self.dataset
         df_new=copy.deepcopy(df)
         df_new['MUNICIPALITY'] = df_new['MUNICIPALITY'].map(self.mapping_list)
         df_new['INDICATOR'] = df_new['INDICATOR'].map(self.mapping_list)
         df_new=df_new.dropna()
         df_synth=copy.deepcopy(df_mun)
        

         list_municipality_1=pd.unique(df_synth['Municipality']).tolist()
         list_municipality_2=pd.unique(df_new['MUNICIPALITY']).tolist()
         list_municipality=list(set(list_municipality_1).intersection(list_municipality_2))
         
         for i in list_municipality:
            size_sample_mun=df_synth['Municipality'].value_counts()[i]  #number of people beloging to the same municipality 
            df_salary=df_new[df_new['MUNICIPALITY']==i]
            average=df_salary.loc[df_salary['INDICATOR']=='Average','value'].iloc[0]
            median=df_salary.loc[df_salary['INDICATOR']=='Median','value'].iloc[0]
            P10=df_salary.loc[df_salary['INDICATOR']=='P10','value'].iloc[0]
            P90=df_salary.loc[df_salary['INDICATOR']=='P90','value'].iloc[0]
            sampler = RandomSampler(df_salary,size_sample_mun)
            salary_stats=Statistics(sampler.datafeature,sampler.size_sample,average,median,P10,P90)
            salary_stats.cdf_log_normal()
            X=salary_stats.get_random_sample_from_constructed_distribution()
            salary_list=np.repeat(salary_stats.x,X)  #vector of the reconstructed salary frequency per municipality to compare with the real statistics parameters
            rnd.shuffle(salary_list)
            df_synth.loc[(df_synth['Municipality'] == i), 'Salary'] = salary_list

            df_synth.loc[df_synth["Age"] >= 65 , "Salary"]= 0

        

         
         return df_synth 

    
    
    
    

    
    
    
    
    
    
    
    
    
    
    
        
