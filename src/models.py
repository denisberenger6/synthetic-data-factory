#!.env/bin python3
"""
Module for SDF models

A module to define the models that are used in the application.
This is relevant for data validation using the Pydantic package for instance.
"""
__version__ = '0.1.0'
__author__ = 'Kerstin Neininger'

from pydantic import BaseModel, StrictBool, Field,model_validator,field_validator
from typing import List, Literal, Union,Optional
from enum import Enum, unique

import utilities as util
from pydantic_core.core_schema import FieldValidationInfo




class ExtendedEnum(Enum):
    @classmethod
    def values(cls):
        """Returns the enum values 

        Returns:
            list: The values of the enum
        """
        return list(map(lambda c: c.value, cls))


@unique
class TargetPersonEnum(str, ExtendedEnum):
    """
    Define Enum for SDF targets from Person domain

    Args:
        Enum (Enum): Generic enumeration
    """

    AGE = "age"
    DOB = "dob"
    MATRICULE = "matricule"
    GEO = "geo"
    NATIONALITY = "nationality"
    NAME = "name-surname"
    SALARY = "salary"
    PHYSICAL_ATTRIBUTES="physical-attributes" 


@unique
class TargetEnergyEnum(str, ExtendedEnum):
    """
    Define Enum for SDF targets from Energy domain

    Args:
        Enum (Enum): Generic enumeration
    """

    SMARTGRID = "smartgrid"
    TBD = "tbd"


@unique
class DomainEnum(str, ExtendedEnum):
    """
    Define Enum for SDG domains
    `Pydantic enums and choices <https://docs.pydantic.dev/latest/usage/types/#enums-and-choices>`_

    Args:
        Enum (Enum): Generic enumeration
    """
    PERSON = "person"
    ENERGY = "energy"


class Source(BaseModel):
    """The SDF `Source` model (configuration file)

    Args:
        BaseModel (object): pydantic base model

    Returns:
        str: The `data` value

    Raises:
        ValueError: If the `data` is not a valid UUID
    """
    type: Literal['external', 'calculated']
    data: str   # Should be a UUID (see @validator)
    dependencies: list

    @field_validator('data')
    def is_uuid(cls, v, values, **kwargs):
        if len(v) != 0 and not util.is_valid_uuid(v):
            raise ValueError(f"Invalid UUID given {v=}.")
        return v




class Generator(BaseModel):
    """The SDF `Generator` model (configuration file)

    Args:
        BaseModel (object): pydantic base model
    """
    method: Literal['sampling', 'rule']
    model: Literal['real_distr','statistics','logic']
    # ge='greater than or equals to', le='less than or equals to'
    nrows: int = Field(None, ge=1, le=10000000)
    
    


class Population(BaseModel):
    """The SDF `Generator` model (configuration file)

    Args:
        BaseModel (object): pydantic base model
   """
    pop_gender: Literal['M','F','mixed']
    age_min_F: int = Field(None, ge=1, le=100)
    age_max_F: int = Field(None, ge=1, le=100)
    age_min_M: int = Field(None, ge=1, le=100)
    age_max_M: int = Field(None, ge=1, le=100)

    @model_validator(mode='after')
    def pop_gend_check(self)->'Population':
        if self.pop_gender == 'mixed' and (self.age_min_F >= self.age_max_F or self.age_min_M >= self.age_max_F):
            raise ValueError (f"Invalid mixed range population {self.age_min_F= } not smaller {self.age_max_F=} and/or {self.age_min_M= } not smaller {self.age_max_M=}")
        if self.pop_gender == 'M' and (self.age_min_M >= self.age_max_M):
            raise ValueError (f"Invalid M range population {self.age_min_M= } and  {self.age_max_M= }")  
        if self.pop_gender == 'F' and (self.age_min_F >= self.age_max_):
            raise ValueError (f"Invalid F range population {self.age_min_F= } and  {self.age_max_F= }") 
        return self
   
        
        

        



class Export(BaseModel):
    """The SDF `Export` model (configuration file)

    Args:
        BaseModel (object): pydantic base model
    """
    save_to_file: StrictBool    # Only allow True/False and not 1 or 0 that are cast into a boolean


class Instruction(BaseModel):
    """The SDF `Instruction` model (configuration file)

    Args:
        BaseModel (object): pydantic base model

    Returns:
        str: The `target` value

    Raises:
        ValueError: If `domain` and `target` do not match.
    """
    index: int
    domain: DomainEnum
    target: Union[TargetPersonEnum, TargetEnergyEnum]
    source: Source
    generator: Generator
    prop_pop : Optional[Population] = None
    export: Export
    
    
    @field_validator('prop_pop')
    def prevent_none(cls, v):
        assert v is not None, 'size may not be None'
        return v
    
    @model_validator(mode="after")
    def pop_gend_check(self):
        return self

    @field_validator('target')
    def domain_target_match(cls, v,info: FieldValidationInfo)-> str:
        if "domain" in info.data:
            domain = info.data["domain"]
            target = v.value
            # Get allowed targets for the given domain
            # Use TargetPersonEnum._member_names_ to get the keys
            # TODO improve this function in case more if statements will be needed
            allowed_targets = TargetPersonEnum.values() if domain == "person" else TargetEnergyEnum.values()
            if v not in allowed_targets:
                raise ValueError(f"The {domain=} and {target=} do not match.")
        return v


class Instructions(BaseModel):
    """The SDF `Instructions` model (configuration file)

    Args:
        BaseModel (object): pydantic base model
    """
    id: str  # UUID #TODO define UUID?
    instructions: List[Instruction]
# Might be useful later (Pydantic package):
# Arguments to constr
# https://docs.pydantic.dev/latest/usage/types/#arguments-to-constr


"""
try:
    Instructions(**instructions)
except ValidationError as err:
    print(err.json(indent=4))

"""

