#!.env/bin python3
"""Utilities Module

A module to define functions for the performance of various tasks such as handing 
and retrieving data from arrays, saving data or generating UUIDs.
"""
from uuid import UUID
import datetime
import random
import pandas as pd
import uuid
__version__ = '0.1.0'
__author__ = 'Kerstin Neininger'

import logging
logger = logging.getLogger(__name__)


def get_index_from_id(id: str, array: list) -> int:
    """Returns the list index of the dictionary with key matching *id*.

    Args:
        id (str): An *id* such as the dataset UUID, which is defined in the request JSON file
        array (list): A list of dictionaries which in turn have a key *id*

    Raises:
        ValueError: Dataset with *id* not found.

    Returns:
        int: The array index matching with the entry of *id*, if the id was found.
    """
    for i in range(0, len(array)):
        dataset_id = array[i]["id"]
        if id == dataset_id:
            return i
    raise ValueError(f"Dataset with {id=} not found.")


def save_xml_file(path: str, filename: str, content: bytes) -> None:
    """Saves byte data as XML file.

    Args:
        path (str): The path to the output folder.
        filename (str): The name of the output file.
        content (bytes): XML content (e.g. as retrieved from STATEC via an API request).
    """
    try:
        if ".xml" not in filename:
            filename += ".xml"
        file = open(path+filename, "wb")
        file.write(content)
        file.close()
    except Exception as err:
        logger.error(f"{err=}, {type(err)=}")
        raise


def pandas_to_pickle(path: str, filename: str, df: pd.DataFrame) -> None:
    """Saves pd.DataFrame object in .pkl format.

    Args:
        path (str): The path to the output folder
        filename (str): The name of the output file
        df (pd.DataFrame): Pandas DataFrame to be stored
    """
    try:
        df.to_pickle(path + filename + ".pkl")
    except Exception as err:
        logger.error(f"{err=}, {type(err)=}")
        raise

def pandas_to_csv(path: str, filename: str, df: pd.DataFrame) -> None:
    """Saves pd.DataFrame object in .csv format.

    Args:
        path (str): The path to the output folder
        filename (str): The name of the output file
        df (pd.DataFrame): Pandas DataFrame to be stored
    """
    try:
        df.to_csv(path + filename + ".csv")
    except Exception as err:
        logger.error(f"{err=}, {type(err)=}")
        raise

def pickle_to_pandas(path: str, filename: str) -> pd.DataFrame:
    """Opens pd.DataFrame object that was stored in .pkl format.

    Args:
        path (str): The path to the output folder.
        filename (str): The name of the output file.

    Returns:
        pd.DataFrame: The pandas dataframe.
    """

    try:
        return pd.read_pickle(path + filename + ".pkl")
    except Exception as err:
        logger.error(f"{err=}, {type(err)=}")
        raise


def generate_uuid() -> str:
    """Generates a random UUID (version 4).

    Returns:
        str: The randomly generated UUID.
    """

    return str(uuid.uuid4())


def generate_random_date(year: str) -> datetime:
    """Generates a random date based on a given year.

    Args:
        year (str): The year

    Returns:
        datetime: The random date.
    """
    try:
        # Calculate random date
        return datetime.datetime.strptime('{} {}'.format(generate_random_integer(1, 366), year), '%j %Y')

    except ValueError:
        # Catch if the value is in the leap year range and calculate again
        generate_random_date(year)


def generate_random_integer(start: int, end: int) -> int:
    """Generates a random integer in the given range.

    This is needed as e.g. the matricule can have a leading zero which is not feasible by randomly sampled integer.

    Args:
        start (int): The lower range.
        end (int): The upper range.

    Returns:
        int: The randomly generated number.
    """
    return random.randint(start, end)


def is_valid_uuid(uuid: str, version: int = 4) -> bool:
    """Checks if the given string is a valid UUID.

    Args:
        uuid (str): The string that is validated.
        version (int, optional): UUID version. Defaults to 4.

    Returns:
        bool: `True` if the given string is a valid UUID, `False` otherwise.
    """
    try:
        uuid_obj = UUID(uuid, version=version)
    except ValueError:
        return False
    return str(uuid_obj) == uuid


def http_request_was_successful(status_code: int) -> bool:
    """Checks whether a specific HTTP request has been successfully completed based in the HTTP response status codes.

    Args:
        status_code (int): HTTP response status codes

    Returns:
        bool: `True` if the HTTP request has been successfully completed, `False` otherwise.
    """
    return status_code >= 200 and status_code < 300
