#!.env/bin python3
"""
Random Sampler Module

A module to sample data from calculated data distributions. Thus, this module creates synthetic data based on random sampling.
"""
__version__ = '0.1.0'
__author__ = 'Kerstin Neininger, Name2'

from typing import Callable, Any, Iterable
import utilities as util
import math as mt

import logging
logger = logging.getLogger(__name__)

import numpy as np 

# TODO: integrate random sampling functions (merge)
class RandomSampler:
    """
     A class to generate a synthetic sample from randomly extract values from a real frequency feature column by using CDF from the normalised histogram 
    """
    def __init__(self,datafeature:Any,size_sample:int) -> None:
        self.datafeature=datafeature
        self.size_sample=size_sample
        self.cdf_datafeature=np.array([])

    def get_sampled_element(self):
        a=np.random.uniform(0,1)
        return np.argmax(self.cdf_datafeature>=a)



    # TODO: integrate random sampling functions (merge)
    def get_random_sample_from_distribution(self):
        self.cdf_datafeature=np.cumsum(self.datafeature/self.datafeature.sum())
        X=np.zeros(len(self.cdf_datafeature))
        for k in np.arange(self.size_sample):
            a=self.get_sampled_element()
            X[a]+=1
        return X.astype(int)

    def get_random_sample_from_constructed_distribution(self):
        X=np.zeros(len(self.cdf_datafeature))
        for k in np.arange(self.size_sample):
            a=self.get_sampled_element()
            X[a]+=1
        return X.astype(int)
       
        

        # Real data from distribution part
        # df_synth = util.pickle_to_pandas(path="../static/sd_results/", filename="DataFrame_Gender_age")
        # return df_synth


class Statistics(RandomSampler):
    """
     A class to generate a synthetic sample from an analytical reconstructed CDF by assuming log-normal distribution from statistics: Average,Median,P10,P90 and draw a random values from it 
    """
    def __init__(self,datafeature,size_sample,average:int,median:int,P10:int,P90:int) ->None:
        
        
        super().__init__(datafeature,size_sample)
        self.average=average
        self.median=median
        self.P10=P10
        self.P90=P90
        self.erf_array=[]
        self.x=[]

    def cdf_log_normal(self):
        mu=mt.log(self.median)
        sigma_P10=mt.log(np.power(self.median/self.P10, 1/1.2816))
        sigma_P90=mt.log(np.power(self.P90/self.median, 1/1.2816))
        sigma_Average=np.sqrt(2*np.log(self.average)-2*mu)
        sigma=np.mean([sigma_P10,sigma_P90,sigma_Average])
        self.x=np.arange(int(np.exp(mu)/np.exp(2*sigma)),int(np.exp(mu)*np.exp(2*sigma)),500)
        arg=(np.log(self.x)-mu)/(sigma*np.sqrt(2))
        self.erf_array = np.asarray([mt.erf(element) for element in arg])
        
    def get_random_sample_from_constructed_distribution(self):
        cdf_log_norm_fun=1/2*(1+self.erf_array)
        self.cdf_datafeature=cdf_log_norm_fun
        X=super().get_random_sample_from_constructed_distribution()
        return X
            
"""""    
data=[]
for i in range(len(X)):
    data.extend(repeat(x[i], X[i]))
"""""
   

        
        
        
        
        
        
        
        
        
  