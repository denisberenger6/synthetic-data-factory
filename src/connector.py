#!.env/bin python3
"""Connector Module

A module to connect to external services.

This can be used to request source data and/or a distribution from these external resources.
Preferably access is granted by a REST API. The module defines classes:

- ``Statec`` class to request and process SDMX data from `LUSTAT-STATEC <https://lustat.statec.lu/>`_ using their `API <https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/typical-use-cases/>`_.
- {class name} class that...
"""
from urllib.request import HTTPError
import sdmx
import pandas as pd
import utilities as util
import os
import json
import requests
__version__ = '0.2.0'
__author__ = 'Kerstin Neininger'

import logging
logger = logging.getLogger(__name__)


# Open JSON file with parameters and query needed to request a specific dataset
with open("../static/request_dataset.json") as file:

    """The JSON file to store request parameters and query for all external services."""
    REQUEST_JSON = json.load(file)

    """Dictionary with information for requesting records from external services."""
    REQUEST_DATASET_JSON = REQUEST_JSON["datasets"]


class Statec:
    """A class to connect to STATEC external service.

    It defines functions to query and process data from STATEC. Available data are in `SDMX <https://sdmx.org/>`_ format.

    :param request_id: The dataset UUID as defined in the request file ``REQUEST_DATASET_JSON``
    :type request_id: str
    """

    BASE_URL = "https://lustat.statec.lu/rest/"
    """The base URL to query the STATEC database."""

    LOCAL_EXPORT_PATH = "../static/LUSTAT_STATEC/"
    """The local path to the output folder where requested data will be stored."""

    ALLOWED_SCOPE = ["query", "structure"]
    """The allowed request scopes as defined by STATEC.
    The allowed values are based on LUSTAT-STATEC REST API options
    """

    def __init__(self, request_id: str) -> None:
        """Constructor method.

        Args:
            request_id (str): The dataset UUID as defined in the request file ``REQUEST_DATASET_JSON``.
        """
        self.request_id = request_id

    def request(self, endpoint: str, params: dict) -> dict:
        """Sends a GET request to the STATEC REST API.

        The LUSTAT-STATEC API documentation can be found `here <https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/typical-use-cases/>`_.
        Note: only GET requests are allowed to query data from this external data source.

        Args:
            endpoint (str): The URL endpoint of the requested resource.
            params (dict): Request parameters.

        Returns:
            dict: The query response stating status, content and the request URL.

        Raises:
            HTTPError: If the HTTP request has not been successfully completed.
            Exception: If the request was not successful or the result was not valid.
        """
        logger.info("Request data")
        try:
            url = self.BASE_URL + endpoint
            response = requests.get(url, params=params)
            result = {"response": response, "status_code": response.status_code,
                      "content": response.content, "url": response.request.url}
            # Raise error if request not successful
            response.raise_for_status()
            return result
        except requests.exceptions.HTTPError as err:
            logger.error(f"{err=}, {type(err)=}")
            raise
        except Exception as err:
            logger.error(f"{err=}, {type(err)=}")
            raise

    def is_reachable(self) -> bool:
        """Verify that the REST API is reachable.

        For simplicity, the request is sent to the 'dataflow' endpoint.

        Returns:
            bool: `True` if the response status code equals 200, `False` otherwise.
        """
        try:
            result = self.request(endpoint="dataflow/", params={})
            return result["status_code"] == 200
        except Exception as err:
            logger.error(f"{err=}, {type(err)=}")
            # raise

    def data_exists(self, scope: str) -> bool:
        """Checks if SDMX dataset already exists in the local folder.

        Args:
            scope (str): The request scope. Allowed values: ``self.ALLOWED_SCOPE``.

        Returns:
            bool: `True` if the SDMX data for the requested resource exists, `False` otherwise.
        """
        try:
            return os.path.exists(f"{self.LOCAL_EXPORT_PATH}{self.request_id}_{scope}.xml")
        except Exception as err:
            logger.error(f"{err=}, {type(err)=}")
            # raise

    def get_datasets_request_information(self) -> dict:
        """Returns the dataset request information as defined in the request file ``REQUEST_DATASET_JSON``.

        Returns:
            dict: Request information for the dataset with this UUID (e.g. query and parameters).
        """
        index = util.get_index_from_id(self.request_id, REQUEST_DATASET_JSON)
        dataset = REQUEST_DATASET_JSON[index]
        return dataset

    def generate_request_data_structure(self, dataset: dict) -> tuple[str, dict]:
        """Returns all information needed for the API data `structure` request.

        The endpoint for this API request is `dataflow/` followed by agency ID, dataflow ID, and version.
        The necessary information is encoded in a request file ``REQUEST_DATASET_JSON``.
        Example URL: https://lustat.statec.lu/rest/dataflow/LU1/DF_B3001/1.0?references=codelist
        Please note that specifying `references=all` results in a XMLParseError() (library pandasdmx).

        Args:
            dataset (dict): Request information for the dataset with this UUID (e.g. query and parameters).

        Returns:
            tuple[str, dict]: API request endpoint and parameters.
        """
        query = dataset["request"]["query"]
        params = dataset["request"]["params"]["data_structure"]

        endpoint = "dataflow/{agencyID}/{dataflowID}/{version}".format(
            agencyID=query["agencyID"],
            dataflowID=query["dataflowID"],
            version=query["version"]
        )
        return endpoint, params

    def generate_request_data_query(self, dataset: dict) -> tuple[str, dict]:
        """Returns all information needed for the API data `query` request.

        The endpoint for this API request is `data/` followed by agency ID, dataflow ID, version, component ID, and frequency of the data.
        The necessary information is encoded in a request file ``REQUEST_DATASET_JSON``.
        Example URL: https://lustat.statec.lu/rest/data/LU1,DF_B3001,1.0/Q.?startPeriod=2015-Q1&endPeriod=2022-Q4&dimensionAtObservation=AllDimensions

        Args:
            dataset (dict): Request information for the dataset with this UUID (e.g. query and parameters).

        Returns:
            tuple[str, dict]: API request endpoint and parameters.
        """
        query = dataset["request"]["query"]
        params = dataset["request"]["params"]["data_query"]

        endpoint = "data/{agencyID},{dataflowID},{version}/{componentID}...{frequency}".format(
            agencyID=query["agencyID"],
            dataflowID=query["dataflowID"],
            version=query["version"],
            componentID=query["componentID"],
            frequency=query["frequency"]
        )
        return endpoint, params

    def get_request_information(self, scope: str, dataset: dict) -> tuple[str, dict]:
        """Returns all information (query and parameters) needed for the API request.

        Depending on the *scope*, different endpoints and parameters are returned.
        This information is encoded in a request file ``REQUEST_DATASET_JSON``.

        Args:
            scope (str): The request scope. Allowed values: ``self.ALLOWED_SCOPE``.
            dataset (dict): Request information for the dataset with this UUID (e.g. query and parameters).

        Raises:
            ValueError: If the `scope` does not match the allowed values.

        Returns:
            tuple[str, dict]: API request endpoint and parameters.
        """
        if scope == "query":
            return self.generate_request_data_query(dataset)
        elif scope == "structure":
            return self.generate_request_data_structure(dataset)
        else:
            raise ValueError(
                f"Only values {self.ALLOWED_SCOPE=} are allowed.")

    def sdmx_to_pandas_df(self) -> pd.DataFrame:
        """Returns query data as pandas DataFrame from SDMX data.

        Examples:
            >>> statec = Statec(id="b1504fde-6242-4240-b107-90e6ad8b014a")
            >>> df_query = statec.sdmx_to_pandas_df()
            INDEX   TIME_PERIOD COUNTRY_BIRTH  SEX AGE_GROUPS FREQ     value
            0         2011           C01  S01        L01    A  512353.0
            1         2011           C01  S01       SL17    A   19728.0
            2         2011           C01  S02       SL12    A   15601.0
            The first three rows are shown.

        Returns:
            pd.DataFrame: Query data from STATEC as data source.
        """
        file = f"{self.LOCAL_EXPORT_PATH}{self.request_id}_query.xml"
        flow_msg = sdmx.read_sdmx(file, format="XML")
        
        df = sdmx.to_pandas(flow_msg)
        df = df.to_frame().reset_index() 
        return df

    def get_sdmx_statistics(self) -> pd.DataFrame:
        """Returns the requested statistics table.

        Returns:
            pd.DataFrame: The statistics table in SDMX format converted to a Python object.
        """
        return self.sdmx_to_pandas_df()

    def sdmx_to_dictlike(self) -> dict:
        """Returns SDMX `structure` as dict-like (as defined by pandasdmx library) data.

        The dictionary can contain keys such as msg_dictlike.keys(): dict_keys(['codelist', 'dataflow', 'structure']).
        The type is type(msg_dictlike): <class 'pandasdmx.util.DictLike'>.

        Returns:
            dict: Structure SDMX information converted to a Python object.
        """
        
        file = f"{self.LOCAL_EXPORT_PATH}{self.request_id}_structure.xml"
        flow_msg = sdmx.read_sdmx(file, format="XML")
        msg_dictlike = sdmx.to_pandas(flow_msg)

        return msg_dictlike

    def get_dataflow(self) -> pd.DataFrame:
        """Returns SDMX `dataflow` as pandas DataFrame from SDMX structure data.

        Returns:
            pd.DataFrame: Dataflow SDMX information converted to a Python object.
        """
        msg_dictlike = self.sdmx_to_dictlike()
        dataflow = msg_dictlike["dataflow"]
        return dataflow.to_frame()

    def get_structure(self) -> pd.DataFrame:
        """Returns `structure` as pandas DataFrame from SDMX structure data.

        Returns:
            pd.DataFrame: Structure SDMX information converted to a Python object.
        """
        msg_dictlike = self.sdmx_to_dictlike()
        structure = msg_dictlike["structure"]
        return structure.to_frame()

    def get_codelist(self) -> dict[str, pd.DataFrame]:
        """Returns `codelist` from SDMX structure data.

        The type of codelist is <class 'pandasdmx.util.DictLike'> (type(codelist)).

        Examples:
            >>> statec = Statec(id="b1504fde-6242-4240-b107-90e6ad8b014a")
            >>> codelist = statec.get_codelist()
            >>> codelist.keys()
            dict_keys(['CL_B1109_AGE_GROUPS', 'CL_B1109_COUNTRY_BIRTH', 'CL_B1109_SEX', 'CL_OBS_STATUS', 'CL_DECIMALS', 'CL_FREQ'])
            >>> df = codelist["CL_B1109_SEX"]
            name        parent
            CL_B1109_SEX                          
            S01           Both sexes  CL_B1109_SEX
            S02                Males  CL_B1109_SEX
            S03              Females  CL_B1109_SEX
            Name: name, parent: float64
            >>> type(df)
            <class 'pandas.core.frame.DataFrame'>

        Returns:
            dict[str, pd.DataFrame]:  Codelist SDMX information converted to a Python object.
        """
        msg_dictlike = self.sdmx_to_dictlike()
        # type(codelist):
        codelist = msg_dictlike["codelist"]
        return codelist

    def get_codelist_mapping(self) -> dict[str,str]:
        """Returns dictionary with all codes based on the codelist.

        Examples:
            >>> {'L01': 'All ages', 'SL01': '0-4 years',...}
            >>> {'CM02001': 'Bettendorf', 'CM02002': 'Bourscheid', 'CM02003': 'Diekirch',...}

        Returns:
            dict[str,str]: Statec codes for this dataset
        """
        codelist = self.get_codelist()
        codelist_mapping = dict()
        for k, v in codelist.items(): 
            for i, l in v.items():
                codelist_mapping[i] = l  
        return codelist_mapping
    
    def request_datasets(self) -> None:
        """Sends a request to the STATEC REST API and stores the SDMX response as XML file.

        If the dataset with this UUID does not yet exist in XML format, the request information is generated.
        The necessary request information is available in the request file ``REQUEST_DATASET_JSON`` and is retrieved base on the provided scope.

        The request scope is defined in allowed values: ``self.ALLOWED_SCOPE``.
        In principle, the statistics dataset and the corresponding codelist, structure etc. are retrieved via two different endpoints.
        """
        # try:
        for scope in self.ALLOWED_SCOPE:
            logger.info(
                f"Request dataset: LUSTAT-STATEC API dataset request for {scope=}")
            # Request data
            id_scope = self.request_id + "_" + scope
            exists = self.data_exists(scope=scope)
            if not exists:
                request_dataset = self.get_datasets_request_information()

                # Get parameters
                endpoint, params = self.get_request_information(
                    scope=scope, dataset=request_dataset)

                # Do request
                result = self.request(endpoint=endpoint, params=params)

                logger.info(
                    f"LUSTAT-STATEC API dataset request {scope=}: Data was requested for {self.request_id=}: {result['url']}")

                # Save as xml file
                util.save_xml_file(
                    path=self.LOCAL_EXPORT_PATH, filename=f"{id_scope}.xml", content=result["content"])
            else:
                logger.info(
                    f"LUSTAT-STATEC API dataset request {scope=}: Data already exists for {self.request_id=}")

        # except Exception as err:
        #    logger.error(f"{err=}, {type(err)=}")
        #    raise



