Project setup
=============

To get started with the SDF, please follow the descriptions in the next section (quick start) or install the virtual environment, install the requirements, and create the necessary subfolders manually.

Quick start
-----------

A bash script can be used to conveniently install all components that are needed for the SDF to function locally:

- Create (sub)folders, e.g., for static files or pipeline results.
- Install the virtual environment.
- Install requirements.

Dependencies to run the script on macOS:

- virtualenv: ``pip install virtualenv``

Run the bash script by executing the following command (make sure dependencies were installed, see above):

.. code-block:: console

    cd project/
    ./install-sdf.sh

The bash script was developed and optimised for macOS. In case of any trouble with the script, it is of course also possible to install the necessary components separately without using the script. The single steps are listed in the next sections.

Install requirements (virtual environment)
------------------------------------------

Setup a virtual environment and install the requirements. Please note that this step is not needed in case the ``install-sdf.sh`` bash script was executed successfully.

.. code-block:: console

    cd project/
    virtualenv env --python=python3
    env/bin/pip3 install -r requirements.txt
