Architecture
------------

The SDF workflow is based on a modular approach and class integration for source data retrieval (datasets or statistics), random data sampling, quality control, model definitions, and generation of synthetic data based on the features to generate. 

The SDF is based on the following core features:

- **Modules**: The current implementation of the SDF is based on several modules and classes (e.g., request/connector, quality control, synthetic data generation, pipeline, distribution generator), which combined allow the generalization of synthetic data depending on the data source and the dependencies with other external sources or some data already generated in the previous steps of the pipeline which pass the single instruction one at a time in a sequential logical way. Data generated can be based on sampling from real discrete distributions, from analytically reconstructed ones or logic rules (dob→matricule e.g.).
- **Command line**: The SDF can be called via a command line, which also allows piping based on a pre-defined configuration/instructions JSON file.
- **Logging**: Logging functionalities were implemented, and loggers as defined in the ``../config/logging.dev.ini`` configuration.
- **Documentation**: Proper documentation via Docstrings and Sphinx (documentation generator).

Details on module connections 
------------
The main file of sdf is ``sdf.py`` in which the instruction, contained in the config file, is imported. The ``quality_control.py`` file contains the class ``QualityController`` called for checking that all the instruction inputs are in the right format and respect the imposed constrains.
When the control phase is passed, the sdf pipeline starts. The instructions are consequently ingested and processed. The class ``SyntheticDataGenerator`` is initialized with the current single instruction-step determined by the ``index`` parameter.
This class analyses the instruction parameters. The function ``provide_source_data`` retreives an external dataset from ``STATEC`` database with an ad-hoc API request built based on the unique UUID which specifices the needed dataset. In case the instruction key ``source-dependencies`` is not empty (should contains a list of ``domain-target`` strings), the function fetches the corresponding stored datasets.
Once the datasets are filled in the appropriate variables,  the function ``generate_synthetic_data`` leads the data towards specific function aimed to generate the new synthetic feature. 
The direction is first given by the ``generator-method``, ``generator-model`` specificity and ``source-dependencies`` list and then conducts to different functions according the ``domain`` and ``target`` that identify uniquely the synthetic generation procedure.
In all the cases, the class ``DistributionGenerator`` is initialized with a dictionary data structure holding the external dataset (``statistics``), the mapping list for decoding labels (``codelist_mapping``) and the sample size (``size_sample``) keys.   
The first data generation is the ``person-age`` and this dataset requires to access other key-values (``prop_pop``) from the instruction. The subclass ``SyntheticDataGeneratorAge`` allows to encorporate the gender and age range information as additional parameters accessible to the class function ``Age_dataset``. 
The function ``Trasform_DataFrame`` in the ``DistributionGenerator`` reshapes the intermediate dataset into another having individual profiles across the rows. Then the length of the output which is the ``person-age`` dataset will be equal to the ``size_sample`` parameter.
The ``random_sampling`` function is used to initialize the ``RandomSampler`` class and provide the random sampling from the real discrete age histograms per gender.
The function ``municipality`` has as entry variable the previous dataset and adds to it the municipality and canton features. The function ``salary`` requires the ``person-geo`` dataset and returns it with the salary feature: for each municipality, the class ``RandomSampler`` is initialized with the dataframe containing just the statistical information of the municipality-related distribution. 
The subclass ``Statistics`` is called for reconstructing the Cumulative Density Function from the average, median, standard deviation, P10 and P90 percentiles by the function ``cdf_log_normal``, and for generating samples from it by the function ``get_random_sample_from_constructed_distribution``. The output ``X`` is a frequency vectors of the discretized salary values in the constructed distribution domain. Then a shuffled slaary list is created to complete the salary feature for the people living in the same municipality.
When the ``source-type`` is ``calculated`` and not ``external``, the ``source_data`` is the dataset or combination of datasets already generated and stored in the ``sd_results`` folder. In this case the function ``apply_rule`` calls specific functions or methods (``multi_dependencies``) dependending on the ``target``. 
A summary graph for module dependencies can be found in the following picture:

.. image:: /architecture_modules.png
 
