.. Synthetic Data Factory (SDF) documentation master file, created by
   sphinx-quickstart on Mon May  8 12:12:53 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

The Synthetic Data Factory (SDF) is a command line tool that has been developed by LNDS (Luxembourg National Data Service) to generate synthetic data from the current population structure in Luxembourg based on different inputs such as real discrete distribution or statistical information. 
The source used to build up the artificial dataset is STATEC `Link <https://lustat.statec.lu/?lc=en&ac=true>`_, the government statistics service of Luxembourg.

It is an open-source initiative where everyone is welcome to contribute by potentially adding more and more features to our ever-growing dataset that can incorporate environmental, health, and economic data. 
It can be potentially extended to multiple domains where LNDS is active and might touch. Synthetic data is ideal for testing systems, software, algorithms, data processing pipelines, and visualization tools. 

The SDF enables users to generate a customized synthetic dataset from the Luxembourg population by fixing three initial and other statistical parameters inside functions for tuning variability. It provides a systematic and ordered generation of features that enrich a Luxembourg cohort, embedding the same statistical properties of the current stage of Luxembourg as a dynamic entity. Indeed, the dataset could also be potentially used to predict a possible future state of our country by tuning the population size, the urban development, and the business impact of companies. One SDF usage that we see is searching for the best socio-economic conditions to improve living conditions for the benefit of the entire society.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   methodology
   architecture
   project-setup
   command-line-usage
   dependencies
   information-for-developers
   modules
   support

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
