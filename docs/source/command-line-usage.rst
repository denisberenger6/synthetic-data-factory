Command line usage
==================

The SDF needs one parameter ``-i`` to specify the data generation configuration. The configuration is a JSON file in a pre-defined format (see below). The synthetic data is generated based on this configuration file, or rather the instructions defined therein.

The configuration also allows for defining multiple piping steps and dependencies between the different datasets created at any step of the entire instruction. The first feature has the target ``age``; the users can generate their base dataset based on the age range and the gender type they decide. Some features like ``Date_of_birth`` and ``Social_matricule`` are created following logic rules. 

Notably, the number of rows (``nrows``) has to be specified and be the exact integers for any chained instructions in the configuration file. 

Command line example
---------------------

Generally, all commands can be displayed using the ``help`` parameters. For example, to get more details on the pipeline or utilities that can be called via the command line, use the following commands:

.. code-block:: console

    # Details on SDF command line parameters. This will list the positional arguments (e.g. {pipeline,utilities})
    python3 sdf.py --help

    # Details on the pipeline feature (core feature of the SDF)
    python3 sdf.py pipeline --help

    # Details which functions in the scope of utilities can be called (additional support for a user)
    python3 sdf.py utilities --help

Here are two command line examples to submit a data generation configuration file and generate synthetic data from these instructions.

Activate the virtual environment (assumption: a virtual environment was installed in the main folder) and then call the SDF using the command line. For instance, the following line can be applied to generate several rows of synthetic identification data for ``age`` (within the ``domain: person``):

.. code-block:: console

    # activate the env
    source env/bin/activate

    # after activating the env, call the SDF (two examples are shown below)
    cd src/
    python3 sdf.py pipeline -i ../config/sdf-instructions-person-age-geo-nationality-salary-2023-10-30.json
    python3 sdf.py pipeline -i ../config/sdf-instructions-person-chaining-2023-05-19.json

    # deactivate/leave env
    deactivate

The following command line, or rather the configuration that is given, starts the SDF using the piping approach and combining several steps:

.. code-block:: console

    python3 sdf.py pipeline -i ../config/sdf-instructions-person-chaining-2023-05-19.json

The piping functionality is achieved by defining several instruction steps, which are executed one after the other. The dependencies between the datasets must be given as well.

Please refer to the JSON files themselves to get more information on the source data, models, and parameters. The JSON files can be found in the folder ``../config``. The configuration is based on a specific pre-defined format, which is explained in the next section.

Configuration file and SDF instructions
---------------------------------------

The configuration is based on a specific pre-defined format. In case the provided JSON is not matching this format, the SDF will stop and exit with an error message. To generate synthetic data, the SDF applies one instruction after the other (see above), whereas every instruction generates specific synthetic datasets. There are currently three main sections to define an instruction:

- **source**: Specifies where the source data (e.g., statistical distribution) comes from. This can be an API request to an external resource or already generated and available synthetic data stored in the specific folder. The ``dependencies`` contains the extra information from the existing datasets to be imported.  
- **generator**: Specifies the details for generating synthetic data itself, e.g., the method, the model to be used or the number of rows to be generated.
- **export**: Specifies the output parameters.

The following table will define the JSON keys and (allowed) values in more detail. Please note that the nested JSON structure is depicted using a hyphen ("`-`") in the JSON key field. Allowed values are depicted in the last column and `highlighted` respectively.

.. list-table::
   :header-rows: 1

   * - JSON key
     - JSON value
     - Description
     - Details and allowed values
   * - instructions
     - List of instructions
     - This is the main 'entry point' of the SDF with instructions organized as an ordered list. The SDF will chain these instructions one by one.
     - Throughout the single instructions, it is mandatory to be consistent with the ``nrows`` parameter that has to be set equal for any step. Moreover, the female and male age ranges must be inserted, even though the population is not mixed. The code will consider just the related age range. 
 

   * - domain
     - string (pre-defined values)
     - The area or ``domain`` for which synthetic data is generated.
     - See below
   * - target
     - string (pre-defined values)
     - The ``target`` data set for which synthetic data is generated in this domain.
     - See below
   * - source-type
     - string (pre-defined values)
     - The source of the data. Can be an API request to an external source or can be based on data that was calculated beforehand (piping).
     - ``external``: the source data is retrieved from an external resource (e.g. via an API request). ``calculated``: the source data comes from already calculated and saved data. 
   * - source-data
     - UUID
     - The UUID refers to external resources and lists all details needed for an API request.
     - Valid UUID
   * - source-dependencies
     - list
     - In case there are several instructions and chaining is applied, the dependencies to other datasets are depicted here.
     -  Here, ``dependencies`` is a list of additional already generated datasets in the format ``domain-target``. 
   * - generator-method
     - string (pre-defined values)
     - Defines how the synthetic data is generated in principle. That means it can be based on a distribution or on previously generated data by following logic rule. 
     - ``sampling``: calculate synthetic data from a real discrete or reconstructed statistical distribution and random sampling from it. ``rule``: calculate synthetic data from a specified rule or clear relationship between features. 
   * - generator-model
     - string (pre-defined values)
     - Define the specific model based on the data input to get the CDF (Cumulative Density Function) before applying the ``sampling`` method to extract random samples. 
     - ``real_distr``: the simple model to create the CDF from the real discrete distribution. ``statistics``: a model for reconstructing the CDF from statistical information such as average, median, and k-th percentiles.
   * - generator-nrows
     - integer
     - The number of synthetically generated data. Thereby, one row is one data point, e.g. one synthetic ``person``.
     - No restrictions for the maximum sample size are imposed but keep in mind the running time. For 1000 people, it is around 1 minute, and for 100.000 people, it takes approximately 40 minutes.
   * - prop_pop-pop_gender 
     - string (pre-defined values)
     - Defines the type of gender population in the sample. 
     - ``F``: total female population. ``M``: total male population. ``mixed``: mixed female and male population. 
   * - prop_pop-age_min(max)_F(M) 
     - integer 
     - Defines the age rage of population. 
     - Both ranges are limited from 0 to 100 years old, and the user must fill in all four parameters to avoid errors in the code.  
   * - export-save_to_file
     - boolean
     - Indicates if the data should be exported. TBD details
     - 

The following Table defines the ``domain`` and ``target`` together with allowed values and a description of which kind of synthetic data is generated in these domains:

.. list-table::
   :header-rows: 1

   * - Domain (allowed values)
     - Scope
     - Target (allowed values)
     - Description
   * - person
     - Identification data
     - age
     - Age and Gender of an identifiable person
   * - person
     - Identification data
     - dob
     - Date of birth of an identifiable person
   * - person
     - Identification data
     - matricule
     - Luxembourg matricule number of an identifiable person
   * - person
     - Identification data
     - geo
     - Canton and municipality of residents 
   * - person
     - Identification data
     - nationality
     - Nationality assigned by overall percentage of foreigns in Luxembourg
   * - person
     - Identification data
     - name-surname
     - Random name assignment by country of origin (Nationality)
   * - person
     - Identification data
     - salary 
     - Monthy salary assigned considering the municipality features then it is a geographical random variable. Aged people has zero salary by default
   * - person
     - Identification data
     - physical-attributes  
     - ``Ethnicity``, ``Hair_length`` and ``Hair_color`` are qualitatively assigned by a subjective realistic evaluation
   * - energy
     - Energy data
     - TBD
     - TBD

SDF utilities suite
-------------------

The SDF also implements utilities functionalities for operations that are executed on a regular basis.

For instance, the generation of a random UUID, which is mandatory for every new dataset that is added to the JSON file ``request_dataset.json`` (needed to make requests), takes place frequently. Future: automate the UUID generation within the whole SDF workflow.

Quick guide: generate random UUID to be used in request_dataset.json file
------------------------------------------------------------------------

Here is the workflow/command line call to generate a random UUID (assumption: a virtual environment was installed in the main folder):

.. code-block:: console

    # activate the env
    source env/bin/activate

    # call the SDF (after activating the env) and call the uuid function.
    # this will return a random UUID printed to the console.
    cd src/
    python3 sdf.py utilities -f uuid

    # deactivate/leave env
    deactivate
