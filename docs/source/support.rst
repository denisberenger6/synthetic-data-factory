Support
=======

If you need help or have any queries regarding the project, feel free to contact `Silvia Martina <mailto:silvia.martina@lnds.lu>`_ for assistance. For further clarification or to address specific issues, we recommend the following steps:

- Open an `Issue <https://gitlab.com/lnds-lu/service-library/code/ser-de-01-sdf/issues/new>`_.
- Provide as much context as possible about the issue you are encountering.
- Include relevant project and platform versions (e.g., nodejs, npm, etc.), depending on what seems applicable.

By following these guidelines, it will be easier for the team to understand and address your queries more effectively.
