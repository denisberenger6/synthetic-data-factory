Introduction
------------

The Synthetic Data Factory (SDF) provides a systematic ad-hoc generation of the Luxembourgish population, which embeds the same statistical information of the current population properties. The source used to build up the artificial dataset is mainly `STATEC <https://lustat.statec.lu/?lc=en&ac=true>`_, the government statistics service of Luxembourg.

The cohort dataset is constructed by concatenating features, which are non-identifiable information by design, while the collective characteristics might reflect the overall Luxembourg population structure.

The gathered information is listed below:

1. Population structure by age and sex in the LUSTAT database `Link <https://lustat.statec.lu/vis?fs[0]=Topics%2C1%7CPopulation%20and%20employment%23B%23%7CPopulation%20structure%23B1%23&pg=0&fc=Topics&lc=en&df[ds]=ds-release&df[id]=DF_B1102&df[ag]=LU1&df[vs]=1.0&pd=2015%2C2023&dq=A..&ly[rw]=AGE&ly[cl]=SEX&lo=1>`_.
2. Population by canton and municipality in the LUSTAT database `Link <https://lustat.statec.lu/vis?lc=en&tm=Population%20by%20canton%20and%20municipality&pg=0&df[ds]=ds-release&df[id]=DF_X021&df[ag]=LU1&df[vs]=1.0&pd=2023%2C2023&dq=A.&lb=bt>`_.
3. The average age of the population by municipality as of 08/11/2021 reported in MAP 4 in the STATEC report: "The territorial distribution of the population" `Link <https://statistiques.public.lu/en/publications/series/rp-2021/2023/rp21-03-23.html>`_.
4. "Population by nationalities in detail on 1st January" in the LUSTAT database `Link <https://lustat.statec.lu/vis?pg=0&lc=en&tm=nationality&df[ds]=ds-release&df[id]=DF_B1113&df[ag]=LU1&df[vs]=1.0&pd=2023%2C2023&dq=.A>`_.
5. "Monthly salaries by municipality" in the LUSTAT database `Link <https://lustat.statec.lu/vis?lc=en&pg=0&tm=monthly%20salaries&df[ds]=ds-release&df[id]=DF_C1600&df[ag]=LU1&df[vs]=1.0&pd=2015%2C2023&dq=..A&ly[cl]=INDICATOR&ly[rw]=MUNICIPALITY&lo=1>`_.

This list will grow increasingly by continuously adding features and enlarging our synthetic dataset by using other publicly available datasets in several contexts, such as business, environment, finance, and health, to mirror the Luxembourg state from many points of view. 
This repository contains Python scripts to generate an artificial Luxembourg dataset with realistic PII (Personal Identifiable Information) with the risk of identifying real people close to zero by design. 
To initialize the dataset, the parameter choice concerns gender, age range, and sample size. These initial constraints will affect the successive features built at the top of this core-base dataset. 
Therefore, the user has first to decide the following required parameters:

- The population sample size. 
- The age-gender dependency: mixed or gender-specific population.
- The age range for both genders is from 0 to 100 years old. Because of the coding implementation aspects, inserting both age ranges is mandatory, even if you choose to have an entirely female or male population.

External sources might be required to generate new features in the dataset, or/and already developed datasets can have information to create concatenated unique attributes. Indeed, there are logic relationships or established dependencies within the partial datasets created at any instruction step.  
The code is extendable to incorporate new features by adding methods and models based on the data inputs. The basic structure of the concatenated instructions would remain the same to highlight the logic and the correlated relationship between the newly ingested data from external sources, the already created synthetic features, and the new features that can enrich the dataset.                              