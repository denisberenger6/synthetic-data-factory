Dependencies
============

Python packages
---------------

- **sphinx**
  `sphinx <https://www.sphinx-doc.org/en/master/>`_ is a documentation generator that is used for the technical documentation of SDF's Python source code.
  LICENSE: `BSD <https://github.com/sphinx-doc/sphinx/blob/master/LICENSE>`_

- **sphinx_rtd_theme**
  `sphinx_rtd_theme <https://github.com/readthedocs/sphinx_rtd_theme>`_ is a Sphinx theme that is used especially on `Read the Docs <https://readthedocs.org/>`_.
  LICENSE: `MIT <https://github.com/readthedocs/sphinx_rtd_theme/blob/master/LICENSE>`_

- **requests**
  `requests <https://github.com/psf/requests>`_ is a Python package for HTTP/1.1 requests.
  LICENSE: `Apache License 2.0 <https://github.com/psf/requests/blob/main/LICENSE>`_

- **pandas**
  `pandas <https://github.com/pandas-dev/pandas>`_ is a Python package for handling and analyzing datasets in an easy and flexible way.
  LICENSE: `BSD 3-Clause License <https://github.com/pandas-dev/pandas/blob/main/LICENSE>`_

- **pandaSDMX**
  `pandaSDMX <https://github.com/dr-leo/pandaSDMX>`_ is a Python package for the exchange of statistical data and metadata in SDMX format.
  LICENSE: `Apache License 2.0 <https://github.com/dr-leo/pandaSDMX/blob/master/LICENSE>`_

- **python-dotenv**
  `python-dotenv <https://github.com/theskumar/python-dotenv>`_ is a Python package to read key-value pairs from a .env file and set them as environment variables.
  LICENSE: `BSD 3-Clause License <https://github.com/theskumar/python-dotenv/blob/main/LICENSE>`_

- **pydantic**
  `pydantic <https://github.com/pydantic/pydantic>`_ is a Python package for data validation using Python type hints.
  LICENSE: `MIT <https://github.com/pydantic/pydantic/blob/main/LICENSE>`_

External services
-----------------

- **LUSTAT-STATEC**
  `LUSTAT-STATEC <https://lustat.statec.lu/>`_ is the `Statistics portal <https://statistiques.public.lu/en.html>`_ of the Grand-Duchy of Luxembourg and belongs to the Luxembourg State. It provides public statistics from Luxembourg from various domains such as population, economy or social life. The service offers a REST API.
  LICENSE: `MIT <https://sis-cc.gitlab.io/dotstatsuite-documentation/about/license/>`_
