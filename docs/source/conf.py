# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('../..'))


# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Synthetic Data Factory (SDF)'

# https://github.com/readthedocs/sphinx_rtd_theme/issues/828#issuecomment-541248939
# See css/custom.css file
copyright = ''

author = 'LNDS'
release = '0.0.1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.viewcode',
    'sphinx.ext.napoleon',
    "sphinx.ext.todo"
]

templates_path = ['_templates']
exclude_patterns = []

# Add a list of modules in the autodoc_mock_imports to prevent import
# errors to halt the building process when some external dependencies are not importable at build time.
# This avoids errors such as:
# WARNING: autodoc: failed to import module 'main' from module 'src'; the following exception was raised:
# No module named 'query'
autodoc_mock_imports = ['sdf', 'connector', 'synthetic_data_generator', 'distribution_generator',
                        'utilities', 'pipeline', 'rules', 'quality_control', 'models', '__init__','multi_dependencies', 'random_sampler']


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

# html_theme = 'alabaster'
html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
html_css_files = [
    'css/custom.css',
]
html_logo = "logo.png"
html_theme_options = {
    'logo_only': True,
    'display_version': False,
}
