Information for Developers
==========================

Technical Documentation of source code
--------------------------------------

Documentation of the Python code is based on `sphinx <https://www.sphinx-doc.org/en/master/>`_. **Docstrings** are formatted using the `Google Style Python Docstrings <https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html>`_ convention. All developers working on this project must ensure that all source code (*classes*, *functions*, *parameters* etc.) is well documented and that the **Docstrings** are formatted according to the above style.

Auto-generate 'Read the Docs' code documentation
------------------------------------------------

As mentioned, `sphinx <https://www.sphinx-doc.org/en/master/>`_ is used to generate documentation (see above). For this, all components such as basic configuration and initialisation files for easy usage of Sphinx have already been set up properly. Thus, after any source code documentation is updated, the Sphinx documentation generator can be called via the command line, see below.

When following the Docstrings format, documentation can be updated as follows (assumption: a virtual environment was installed in the main folder):

.. code-block:: console

    # activate the env (installed beforehand)
    source env/bin/activate

    # build html file
    cd docs/
    make clean
    make html

    # deactivate/leave env
    deactivate

Please make sure that all source code follows the Docstrings format to avoid errors in executing the commands above.

If the build was successful, open ``./docs/build/html/index.html`` in a browser to view a nice Read-the-Docs-style documentation.
