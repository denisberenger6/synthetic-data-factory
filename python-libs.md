| Name               | Version      | License                                                                               | URL                                                      |
|--------------------|--------------|---------------------------------------------------------------------------------------|----------------------------------------------------------|
| Faker              | 19.13.0      | MIT License                                                                           | https://github.com/joke2k/faker                          |
| annotated-types    | 0.6.0        | MIT License                                                                           | UNKNOWN                                                  |
| argparse           | 1.4.0        | Python Software Foundation License                                                    | https://github.com/ThomasWaldmann/argparse/              |
| certifi            | 2023.7.22    | Mozilla Public License 2.0 (MPL 2.0)                                                  | https://github.com/certifi/python-certifi                |
| charset-normalizer | 3.3.2        | MIT License                                                                           | https://github.com/Ousret/charset_normalizer             |
| distlib            | 0.3.7        | Python Software Foundation License                                                    | https://github.com/pypa/distlib                          |
| idna               | 3.4          | BSD License                                                                           | https://github.com/kjd/idna                              |
| logging            | 0.4.9.6      | Copyright (C) 2001-2005 by Vinay Sajip. All Rights Reserved. See LICENSE for license. | http://www.red-dove.com/python_logging.html              |
| lxml               | 4.9.3        | BSD License                                                                           | https://lxml.de/                                         |
| numpy              | 1.26.1       | BSD License                                                                           | https://numpy.org                                        |
| pandas             | 2.1.2        | BSD License                                                                           | https://pandas.pydata.org                                |
| pandas-read-xml    | 0.3.1        | MIT License                                                                           | https://github.com/minchulkim87/pandas_read_xml          |
| pip                | 23.3.1       | MIT License                                                                           | https://pip.pypa.io/                                     |
| pip-licenses       | 4.3.3        | MIT License                                                                           | https://github.com/raimon49/pip-licenses                 |
| prettytable        | 3.9.0        | BSD License                                                                           | https://github.com/jazzband/prettytable                  |
| pyarrow            | 14.0.0       | Apache Software License                                                               | https://arrow.apache.org/                                |
| pydantic           | 2.4.2        | MIT License                                                                           | https://github.com/pydantic/pydantic                     |
| pydantic_core      | 2.10.1       | MIT License                                                                           | https://github.com/pydantic/pydantic-core                |
| python-dateutil    | 2.8.2        | Apache Software License; BSD License                                                  | https://github.com/dateutil/dateutil                     |
| pytz               | 2023.3.post1 | MIT License                                                                           | http://pythonhosted.org/pytz                             |
| requests           | 2.31.0       | Apache Software License                                                               | https://requests.readthedocs.io                          |
| scipy              | 1.11.3       | BSD License                                                                           | https://scipy.org/                                       |
| setuptools         | 65.5.0       | MIT License                                                                           | https://github.com/pypa/setuptools                       |
| six                | 1.16.0       | MIT License                                                                           | https://github.com/benjaminp/six                         |
| typing_extensions  | 4.8.0        | Python Software Foundation License                                                    | https://github.com/python/typing_extensions              |
| tzdata             | 2023.3       | Apache Software License                                                               | https://github.com/python/tzdata                         |
| urllib3            | 2.0.7        | MIT License                                                                           | https://github.com/urllib3/urllib3/blob/main/CHANGES.rst |
| wcwidth            | 0.2.9        | MIT License                                                                           | https://github.com/jquast/wcwidth                        |
| xmltodict          | 0.13.0       | MIT License                                                                           | https://github.com/martinblech/xmltodict                 |
| zipfile36          | 0.1.3        | Python Software Foundation License                                                    | https://gitlab.com/takluyver/zipfile36                   |
