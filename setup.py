from setuptools import setup

setup(
    name='synthetic-data-factory',
    description='Synthetic Data Factory: a Python application enabling users to generate synthetic datasets from the Luxembourgish population based on real statistical distribution.',
    version='0.1.0',
    license='Apache 2.0',
)
