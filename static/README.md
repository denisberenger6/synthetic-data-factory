# Static data

In the following, the static files used by the SDF are described.

## REST API request parameters

The `connector` module and the classes defined therein, retrieve all parameters, which are needed to request source data using external REST APIs, from the JSON file `request_dataset.json`. REST APIs requests can be sent to the following external services:

- Statec: request and process SDMX data from [LUSTAT-STATEC](https://lustat.statec.lu/) using their [API](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/typical-use-cases/). SDMX, which stands for `Statistical Data and Metadata eXchange`, is an international ISO standard using XML syntax. SDMX was developed to standardise the mechanisms and processes that are needed for the exchange of statistical data and metadata.

To generate a random UUID, mandatory to add new datasets to the JSON file `request_dataset.json`, please use the SDF utilities suite. More information can be found in the main SDF README.md file.

### LUSTAT STATEC (Luxembourg)

[LUSTAT-STATEC](https://lustat.statec.lu/) is the [Statistics portal](https://statistiques.public.lu/en.html) of the Grand-Duchy of Luxembourg and belongs to the Luxembourg State. It provides public statistics from Luxembourg from various domains such as population, economy or social life. The service offers a REST API.

Using their API, requests can be send to `query` and `structure`, for retrieving statistical data in SDMX format and the corresponding codelist, respectively. Besides source information such as `title` or `description`, the aforementioned JSON file contains information that is needed to send a `request` to the Statec REST API.

For these requests, the following parameters need to be defined:

- query:
    - `dataflowID`: ID of dataflow
    - `agencyID`: Agency of dataflow
    - `version`: Version
    - `componentID`: Used to retrieve the values for one specific dimension only. Can be used for filtering data.
    - `frequency`: frequency parameters (e.g. 'A' for 'Annual', see below)
- params
    - `startPeriod`: Starting time period
    - `endPeriod`: Ending time period
    - `dimensionAtObservation`: Display of results. 'AllDimensions' is used for data visualization page. In this case the API returns a flat list of observations.


For more information, please refer to the official [API documentation](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/typical-use-cases/).

The `Statec` class then provides functionalities to retrieve these parameters and generate the respective request. For instance, a request for the "Total population by sex and age groups and by country of birth 2001, 2011" will be generated as follows:
```
https://lustat.statec.lu/rest/data/LU1,DF_B1109,1.0/C01...A?startPeriod=2011&endPeriod=2011&dimensionAtObservation=AllDimensions
```

Retrieving the structural information for `agencyID=LU1` and `dataflowID=DF_B1109` would result in the following request:

```
https://lustat.statec.lu/rest/dataflow/LU1/DF_B1109/1.0?references=codelist
```

This contains information on the codelist such as:
- 'A' = 'Annual'
- 'C01' =' TOTAL POPULATION'

Please note that setting `"references": "codelist"` to retrieve structure data, instead of `"references": "all"` (as described in the official documentation), is necessary to avoid a XML parsing error in the `pandasdmx` Python package.

In detail, `"references": "all"` gives an error raised by the pandasdmx XML parser:
```
ERROR err=XMLParseError(), type(err)=<class 'pandasdmx.exceptions.XMLParseError'>
```
Thus, it was changed to `"references": "codelist"`.