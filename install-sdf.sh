#! /bin/bash

echo -e "START: SDF setup"

# Create static folders
echo -e "Create static folders"
mkdir -p ./static/LUSTAT_STATEC/;
mkdir -p ./static/sd_results/;
mkdir -p ./logs/;

# Install virtual env
echo -e "Install virtual environment"
virtualenv env --python=python3

# Install requirements
echo -e "Install requirements"
env/bin/pip3 install -r requirements.txt

# Create .env file (used by logger)
# Default: 'dev' development environment
echo "ENV=dev" > .env

# Create Sphinx documentation
echo -e "Create Sphinx documentation"
source env/bin/activate
cd docs/;make clean;make html;cd ..
deactivate

echo -e "END: SDF setup"